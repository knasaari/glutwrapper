#ifndef VEC3_H
#define	VEC3_H

#include <complex>
#include "common.h"

template<typename X>
class Vec3{
	public:
	union{
		X v[4];
		vec4pack<X> m;
	};
	
	Vec3();
	Vec3(X a);
	Vec3(X x, X y, X z);
	Vec3(const vec4pack<X> &other);
	Vec3(const Vec3<X> &other);
	Vec3(const X* a);

	Vec3<X>& operator+=(const Vec3<X> &other);
	Vec3<X>& operator-=(const Vec3<X> &other);
	Vec3<X>& operator*=(const Vec3<X> &other);
	Vec3<X>& operator/=(const Vec3<X> &other);
	
	Vec3<X> operator+(const Vec3<X> &other) const;
	Vec3<X> operator-(const Vec3<X> &other) const;
	Vec3<X> operator-() const;
	Vec3<X> operator*(const Vec3<X> &other) const;
	Vec3<X> operator/(const Vec3<X> &other) const;
	
	bool operator<(const Vec3<X> &other) const;
	bool operator<=(const Vec3<X> &other) const;
	bool operator>(const Vec3<X> &other) const;
	bool operator>=(const Vec3<X> &other) const;
	bool operator==(const Vec3<X> &other) const;
	bool operator!=(const Vec3<X> &other) const;
	bool operator()(const Vec3<X> &first, const Vec3<X>& second) const;
	
	Vec3<X>& operator*=(X mul);
	Vec3<X>& operator/=(X div);
	Vec3<X> operator*(X mul) const;
	Vec3<X> operator/(X div) const;
	
	X length() const;
	X distance(const Vec3<X> &other) const;
	
	/**
	 * Normalizes this vector and returns it as a copy
	 * 
	 * @return the normalized vector
	 */
	Vec3<X> normalize() const;
	
	/**
	 * 
	 * @param other 
	 * @return 
	 */
	Vec3<X> deltaSquared(const Vec3<X> &other) const;
	
	static X dot(const Vec3<X> &left, const Vec3<X> &right);
	static Vec3<X> cross(const Vec3<X> &left, const Vec3<X> &right);
	
	inline X x() const {
		return v[0];
	}
	
	inline X y() const {
		return v[1];
	}
	
	inline X z() const {
		return v[2];
	}
	
	inline void setX(X n){
		v[0] = n;
	}
	
	inline void setY(X n){
		v[1] = n;
	}
	
	inline void setZ(X n){
		v[2] = n;
	}
	
	inline Vec3<X> yzx() const {
		return Vec3<X>(v[1],v[2],v[0]);
	}
	
	inline Vec3<X> zxy() const {
		return Vec3<X>(v[2],v[0],v[1]);
	}
	
	inline void store(X *p) const {
		p[0] = v[0];
		p[1] = v[1];
		p[2] = v[2];
	}
};

template<typename X>
inline Vec3<X> operator*(X mul, const Vec3<X> &v){
	return Vec3<X>(mul * v.x(), mul * v.y(), mul * v.z());
}

template<typename X>
inline Vec3<X> operator/(X div, const Vec3<X> &v){
	return Vec3<X>(div / v.x(), div / v.y(), div / v.z());
}

template<typename X>
inline X sum(Vec3<X> v) {
	return v.x() + v.y() + v.z();
}

template<typename X>
inline X dot(Vec3<X> left, Vec3<X> right){
	return sum(left*right);
}

template<typename X>
inline Vec3<X> cross(Vec3<X> left, Vec3<X> right){
	return (left.zxy()*right - left*right.zxy()).zxy();
}

template<typename X>
inline X length(Vec3<X> v) {
	return std::sqrt(dot(v, v));
}

template<typename X>
inline X distance(Vec3<X> a, Vec3<X> b){
	return std::sqrt(dot(a, b));
}

template<typename X>
inline Vec3<X> normalize(Vec3<X> v){
	float len = length(v);
	if(len == 0.0f){
		return Vec3<X>(0.0f);
	}
	
	return v * (1.0f / length(v));
}
#endif
