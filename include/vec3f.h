#ifndef VEC3F_H
#define	VEC3F_H

#include <complex>
#include <xmmintrin.h>
#include "xsimd/memory/xsimd_aligned_allocator.hpp"
#include "common.h"

#define SHUFFLE3(V, X,Y,Z) SIMDVec3f(_mm_shuffle_ps((V).m, (V).m, _MM_SHUFFLE(Z,Z,Y,X)))

class __attribute__((aligned(16))) SIMDVec3f : public VecfAlign{
public:
	__m128 m;

	inline SIMDVec3f(){
		m = _mm_setzero_ps();
	}

	inline explicit SIMDVec3f(__m128 v){
		m = v;
	}
	
	inline explicit SIMDVec3f(float a){
		m = _mm_set1_ps(a);
	}

	inline explicit SIMDVec3f(float x, float y, float z){
		m = _mm_set_ps(z, z, y, x);
	}

	inline explicit SIMDVec3f(const float* a){
		m = _mm_set_ps(a[2], a[2], a[1], a[0]);
	}
	
	inline float x() const {
		return _mm_cvtss_f32(m);
	}
	
	inline float y() const {
		return _mm_cvtss_f32(_mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 1, 1, 1)));
	}
	
	inline float z() const {
		return _mm_cvtss_f32(_mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 2, 2, 2)));
	}
	
	inline SIMDVec3f yzx() const {
		return SHUFFLE3(*this, 1, 2, 0);
	}
	
	inline SIMDVec3f zxy() const {
		return SHUFFLE3(*this, 2, 0, 1);
	}
	
	inline void store(float *p) const {
		p[0] = x();
		p[1] = y();
		p[2] = z();
	}
	
	/**
	 * Stores all 4 floats to input array.
	 * @param p
	 */
	inline void storeVec4f(float *p) const {
		_mm_storeu_ps(p, m);
	}
	
	void setX(float x){
		m = _mm_move_ss(m, _mm_set_ss(x));
	}
	
	void setY(float y){
		__m128 t = _mm_move_ss(m, _mm_set_ss(y));
		t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 2, 0, 0));
		m = _mm_move_ss(t, m);
	}
	
	void setZ(float z){
		__m128 t = _mm_move_ss(m, _mm_set_ss(z));
		t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 0, 1, 0));
		m = _mm_move_ss(t, m);
	}

	inline bool operator<(const SIMDVec3f &other) const{
		return true;
	}


	inline bool operator<=(const SIMDVec3f &other) const{
		return true;
	}

	inline bool operator>(const SIMDVec3f &other) const{
		return true;
	}


	inline bool operator>=(const SIMDVec3f &other) const{
		return true;
	}


	inline bool operator==(const SIMDVec3f &other) const{
		return true;
	}


	inline bool operator!=(const SIMDVec3f &other) const{
		return true;
	}


	inline bool operator()(const SIMDVec3f& first, const SIMDVec3f& second) const{
		return true;
	}
	
	/**
	 * 
	 * @param other 
	 * @return 
	 */
	SIMDVec3f deltaSquared(const SIMDVec3f &other) const;
};

//#define VCONST extern const __declspec(selectany)
struct vconstu
{
	union { uint32_t u[4]; __m128 v; };
	inline operator __m128() const { return v; }
};

//VCONST vconstu vsignbits = { 0x80000000, 0x80000000, 0x80000000, 0x80000000 };

inline SIMDVec3f operator+(SIMDVec3f a, SIMDVec3f b){
	a.m = _mm_add_ps(a.m, b.m);
	return a;
}

inline SIMDVec3f operator-(SIMDVec3f a, SIMDVec3f b){
	a.m = _mm_sub_ps(a.m, b.m);
	return a;
}

inline SIMDVec3f operator*(SIMDVec3f a, SIMDVec3f b){
	a.m = _mm_mul_ps(a.m, b.m);
	return a;
}

inline SIMDVec3f operator/(SIMDVec3f a, SIMDVec3f b){
	a.m = _mm_div_ps(a.m, b.m);
	return a;
}

// 

inline SIMDVec3f operator*(float a, SIMDVec3f b){
	b.m = _mm_mul_ps(_mm_set1_ps(a), b.m);
	return b;
}

inline SIMDVec3f operator*(SIMDVec3f a, float b){
	a.m = _mm_mul_ps(a.m, _mm_set1_ps(b));
	return a;
}

inline SIMDVec3f operator/(float a, SIMDVec3f b){
	b.m = _mm_div_ps(_mm_set1_ps(a), b.m);
	return b;
}

inline SIMDVec3f operator/(SIMDVec3f a, float b){
	a.m = _mm_div_ps(a.m, _mm_set1_ps(b));
	return a;
}

// ASSIGNMENT ARITHMETICS

inline SIMDVec3f& operator+=(SIMDVec3f& a, SIMDVec3f b){
	a = a + b; return a;
}

inline SIMDVec3f& operator-=(SIMDVec3f& a, SIMDVec3f b){
	a = a - b; return a;
}

inline SIMDVec3f& operator*=(SIMDVec3f& a, SIMDVec3f b){
	a = a * b; return a;
}

inline SIMDVec3f& operator/=(SIMDVec3f& a, SIMDVec3f b){
	a = a / b; return a;
}

inline SIMDVec3f& operator*=(SIMDVec3f& a, float b){
	a = a * b; return a;
}

inline SIMDVec3f& operator/=(SIMDVec3f& a, float b){
	a = a / b; return a;
}

// UNARY

inline SIMDVec3f operator-(SIMDVec3f a){
	return SIMDVec3f(_mm_setzero_ps()) - a; 
}

inline SIMDVec3f abs(SIMDVec3f v) {
	//v.m = _mm_andnot_ps(vsignbits, v.m);
	return v;
}

inline float sum(SIMDVec3f v) {
	return v.x() + v.y() + v.z();
}

inline float dot(SIMDVec3f left, SIMDVec3f right){
	return sum(left*right);
}

inline SIMDVec3f cross(SIMDVec3f left, SIMDVec3f right){
	return (left.zxy()*right - left*right.zxy()).zxy();
}

inline float distance(SIMDVec3f a, SIMDVec3f b){
	return std::sqrt(dot(a, b));
}

inline float length(SIMDVec3f v) {
	return distance(v,v);
}

inline SIMDVec3f normalize(SIMDVec3f v){
	float len = length(v);
	if(len == 0.0f){
		return SIMDVec3f(0.0f);
	}
	return v * (1.0f / length(v));
}

#endif
