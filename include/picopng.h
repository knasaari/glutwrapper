/**
 * Copyright (c) 2005-2016 Lode Vandevenne
 */
#ifndef PICOPNG_H
#define PICOPNG_H

#include <string.h>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

int decodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);

#ifdef __cplusplus
}
#endif

#endif /* PICOPNG_H */

