#include "vec2.h"
#include "vec3.h"
#include "vec3f.h"
#include "vec4f.h"
#include "vec4.h"
#include <vector>

#ifdef SIMD
typedef SIMDVec3f Vec3f;
typedef std::vector<Vec3f,xsimd::aligned_allocator<Vec3f,16>> VectorVec3f;
typedef SIMDVec4f Vec4f __attribute__ ((__aligned__ (16)));
typedef std::vector<Vec4f,xsimd::aligned_allocator<Vec4f,16>> VectorVec4f;
#else
typedef Vec3<float> Vec3f;
typedef std::vector<Vec3f> VectorVec3f;
typedef Vec4<float> Vec4f;
typedef std::vector<Vec4f> VectorVec4f;
#endif

typedef Vec2<float> Vec2f;
typedef Vec2<int> Vec2i;

typedef Vec3<int> Vec3i;

typedef Vec4<int> Vec4i;
