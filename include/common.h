#ifndef COMMONVEC_H
#define COMMONVEC_H

#include <assert.h>
#include <stdlib.h>

#define is_aligned(POINTER, BYTE_COUNT) (((uintptr_t)(const void *)(POINTER)) % (BYTE_COUNT) == 0)

#ifdef MINGW
	#define ALIGNED_MALLOC(S,A) _aligned_malloc(S,A)
	#define ALIGNED_FREE(M) _aligned_free(M)
#else
	#define ALIGNED_MALLOC(S,A) aligned_alloc(A,S)
	#define ALIGNED_FREE(M) free(M)
#endif

class VecfAlign{
	public:
	static const size_t nAlignment = 16;
	
	void * operator new( size_t nSize ) {
		void * ptr = (void *)ALIGNED_MALLOC(nSize, nAlignment);
		assert(is_aligned(ptr, nAlignment));
		return ptr;
	}
	
	void * operator new( size_t nSize, void * ptr) {
		assert(is_aligned(ptr, nAlignment));
		return ptr;
	} 
	
	
	void operator delete( void *pObject) {
		if( pObject != NULL )  
			ALIGNED_FREE( pObject );  
	} 
	
	void *operator new[]( size_t nSize ){
		void * ptr = (void *)ALIGNED_MALLOC(nSize, nAlignment); 
		assert(is_aligned(ptr, nAlignment));
		return ptr;
	}
	
	void *operator new[]( size_t nSize, void * ptr){
		assert(is_aligned(ptr, nAlignment));
		return ptr;
	}  

	void operator delete[]( void *pObject )  {
		ALIGNED_FREE(pObject);
	} 
};

template<typename X>
struct vec4pack{
	X x,y,z,w;
};

#endif /* COMMON_H */

