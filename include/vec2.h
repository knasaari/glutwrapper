#ifndef VEC2_H
#define	VEC2_H

#include <complex>

template<typename X>
class Vec2{
	public:
	union{
		struct{
			X x,y;
		};
		X v[2];
	};
	Vec2();
	Vec2(X a);
	Vec2(X x, X y);
	Vec2(const Vec2<X> &other);
	Vec2(const X* a);
	virtual ~Vec2();

	Vec2<X>& operator+=(const Vec2<X> &other);
	Vec2<X>& operator-=(const Vec2<X> &other);
	Vec2<X>& operator*=(const Vec2<X> &other);
	Vec2<X>& operator/=(const Vec2<X> &other);

	Vec2<X> operator+(const Vec2<X> &other) const;
	Vec2<X> operator-(const Vec2<X> &other) const;
	Vec2<X> operator-() const;
	Vec2<X> operator*(const Vec2<X> &other) const;
	Vec2<X> operator/(const Vec2<X> &other) const;
	
	bool operator<(const Vec2<X> &other) const;
	bool operator<=(const Vec2<X> &other) const;
	bool operator>(const Vec2<X> &other) const;
	bool operator>=(const Vec2<X> &other) const;
	bool operator==(const Vec2<X> &other) const;
	bool operator!=(const Vec2<X> &other) const;
	bool operator()(const Vec2<X> &first, const Vec2<X>& second) const;
	
	Vec2<X>& operator*=(X mul);
	Vec2<X> operator*(const X mul) const;
	
	X length() const;
	X distance(const Vec2<X> &other) const;
	
	/**
	 * Normalizes this vector and returns it as a copy
	 * 
         * @return the normalized vector
         */
	Vec2<X> normalize() const;
	
	/**
	 * Returns the normalized vector of other - this
	 * 
	 * @param other 
	 * @return 
	 */
	Vec2<X> deltaSquared(const Vec2<X> &other) const;
};

#endif
