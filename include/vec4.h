#ifndef VEC4_H
#define	VEC4_H

#include <complex>
#include "common.h"

template<typename X>
class Vec4{
	public:
	union{
		X v[4];
		vec4pack<X> m;
	};
	
	Vec4();
	Vec4(X a);
	Vec4(X x, X y, X z, X w);
	Vec4(const vec4pack<X> &other);
	Vec4(const Vec4<X> &other);
	Vec4(const X* a);

	Vec4<X>& operator+=(const Vec4<X> &other);
	Vec4<X>& operator-=(const Vec4<X> &other);
	Vec4<X>& operator*=(const Vec4<X> &other);
	Vec4<X>& operator/=(const Vec4<X> &other);
	
	Vec4<X> operator+(const Vec4<X> &other) const;
	Vec4<X> operator-(const Vec4<X> &other) const;
	Vec4<X> operator-() const;
	Vec4<X> operator*(const Vec4<X> &other) const;
	Vec4<X> operator/(const Vec4<X> &other) const;
	
	bool operator<(const Vec4<X> &other) const;
	bool operator<=(const Vec4<X> &other) const;
	bool operator>(const Vec4<X> &other) const;
	bool operator>=(const Vec4<X> &other) const;
	bool operator==(const Vec4<X> &other) const;
	bool operator!=(const Vec4<X> &other) const;
	bool operator()(const Vec4<X> &first, const Vec4<X>& second) const;
	
	Vec4<X>& operator*=(X mul);
	Vec4<X>& operator/=(X mul);
	Vec4<X> operator*(X mul) const;
	Vec4<X> operator/(X mul) const;
	
	X length() const;
	X distance(const Vec4<X> &other) const;
	
	/**
	 * Normalizes this vector and returns it as a copy
	 * 
	 * @return the normalized vector
	 */
	Vec4<X> normalize() const;
	
	/**
	 * 
	 * @param other 
	 * @return 
	 */
	Vec4<X> deltaSquared(const Vec4<X> &other) const;
	
	inline X x() const {
		return v[0];
	}
	
	inline X y() const {
		return v[1];
	}
	
	inline X z() const {
		return v[2];
	}
	
	inline X w() const {
		return v[3];
	}
	
	inline void setX(X n){
		v[0] = n;
	}
	
	inline void setY(X n){
		v[1] = n;
	}
	
	inline void setZ(X n){
		v[2] = n;
	}
	
	inline void setW(X n){
		v[3] = n;
	}
	
	inline Vec4<X> yzx() const {
		return Vec4<X>(v[1],v[2],v[0],v[3]);
	}
	
	inline Vec4<X> zxy() const {
		return Vec4<X>(v[2],v[0],v[1],v[3]);
	}
	
	inline Vec4<X> wzyx() const {
		return Vec4<X>(v[3],v[2],v[1],v[0]);
	}
	
	inline Vec4<X> zwxy() const {
		return Vec4<X>(v[2],v[3],v[0],v[1]);
	}
	
	inline Vec4<X> yxwz() const {
		return Vec4<X>(v[1],v[0],v[3],v[2]);
	}
	
	inline void store(X *p) const {
		p[0] = v[0];
		p[1] = v[1];
		p[2] = v[2];
		p[3] = v[3];
	}
};

template<typename X>
inline Vec4<X> operator*(X mul, const Vec4<X> &v){
	return Vec4<X>(mul * v.x(), mul * v.y(), mul * v.z(), mul * v.w());
}

template<typename X>
inline Vec4<X> operator/(X div, const Vec4<X> &v){
	return Vec4<X>(div / v.x(), div / v.y(), div / v.z(), div / v.w());
}

template<typename X>
inline X sum(Vec4<X> v) {
	return v.x() + v.y() + v.z() + v.w();
}

template<typename X>
inline X dot(Vec4<X> left, Vec4<X> right){
	return sum(left*right);
}

template<typename X>
inline Vec4<X> cross(Vec4<X> left, Vec4<X> right){
	return (left.zxy()*right - left*right.zxy()).zxy();
}

template<typename X>
inline X length(Vec4<X> v) {
	return std::sqrt(dot(v, v));
}

template<typename X>
inline X distance(Vec4<X> a, Vec4<X> b){
	return std::sqrt(dot(a, b));
}

template<typename X>
inline Vec4<X> normalize(Vec4<X> v){
	float len = length(v);
	if(len == 0.0f){
		return Vec4<X>(0.0f);
	}
	
	return v * (1.0f / length(v));
}

#endif
