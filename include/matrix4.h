#ifndef MATRIX4_H
#define	MATRIX4_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	union{
		struct{
			float m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33;
		};
		float m[16];
	};
} Matrix4f;

void setZeroMat4(Matrix4f *matrix);
void setIdentityMat4(Matrix4f *matrix);
void rotateMat4(Matrix4f *matrix, float angleRad, float axisX, float axisY, float axisZ);
void translateMat4(Matrix4f *matrix, float x, float y, float z);
void transposeMat4(Matrix4f *matrix);
void invertMat4(Matrix4f *matrix);
float determinant3x3(float t00, float t01, float t02, float t10, float t11, float t12, float t20, float t21, float t22);
float determinantMat4(const Matrix4f* matrix);
void transformMat4(const Matrix4f *left, const float *right, float *dest);
void mulMat4(const Matrix4f *left, const Matrix4f *right, Matrix4f *dest);
void addMat4(const Matrix4f *left, const Matrix4f *right, Matrix4f *dest);
void subMat4(const Matrix4f *left, const Matrix4f *right, Matrix4f *dest);
void scaleMat4(Matrix4f *matrix, float x, float y, float z);
void printMat4(const Matrix4f *matrix);

#ifdef __cplusplus
}
#endif

#endif
