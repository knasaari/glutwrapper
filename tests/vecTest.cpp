#include "vec.h"
#include "../src/utility.h"
#include "../src/Engine/Mesh/Mesh.h"
#include <iostream>
#include <stdexcept>

int main(){

	Mesh mesh;
	
	try{
		mesh.loadObj("../assets/track/track_x_cross.obj");
	} catch(int e){
		printf("Failed to open ../assets/track/track_x_cross.obj");
		return 1;
	}
	
	std::size_t floatCount;
	std::vector<float> buffer;
	mesh.getRawData(buffer, floatCount);
	
	saveCharData("./dumps/dump.txt", buffer.data(),sizeof(float),floatCount);
	
	return 0;
}