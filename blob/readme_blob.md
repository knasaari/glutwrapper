Use **meshmaker** to convert .obj files to .bin blobs.  
`meshmaker convert cup2.obj`

Then use use **ld** to convert the .bin to a linkable format.  
`ld -r -b binary -o cup2.bin.o cup2.bin`
