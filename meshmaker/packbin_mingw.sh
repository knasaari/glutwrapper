#!/bin/bash
cd $(dirname $0)
if [ ! -f $1 ]; then
    echo "File not found!"
    exit 1
fi

FILE="${1%.*}"
./meshmaker.exe convert "${FILE}.obj"
ld --verbose -r -b binary -o "${FILE}.bin.o" "${FILE}.bin" | grep attempt
rm "${FILE}.bin"
