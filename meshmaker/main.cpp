#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <vector>
#include "matrix4.h"
#include "../src/utility.h"
#include "../src/Engine/Mesh/Mesh.h"
#include <stdexcept>

void saveOrigo();

int main(int argc, char** argv) {
	if(argc < 3){
		printf("Usage: meshmaker convert FILE\n\tFILE must be a valid Wavefront OBJ file.\n");
		return 0;
	}
	if(strcmp(argv[1], "convert") == 0){
		Mesh mesh;
		try{
			mesh.loadObj(argv[2]);
		} catch(int e){
			printf("Failed to open %s\n", argv[2]);
			return 1;
		}
		
		int len = strlen(argv[2]);
		char* newfile = new char[len+5];
		memcpy(newfile, argv[2], sizeof(char)*(len));
		char* dot = strrchr(argv[2], '.');
		
		if(dot != NULL){
			int dotPlace = (int)(dot-argv[2]+1);
			newfile[dotPlace++] = 'b';
			newfile[dotPlace++] = 'i';
			newfile[dotPlace++] = 'n';
			newfile[dotPlace++] = '\0';
		}
		else{
			newfile[len] = '.';
			newfile[len+1] = 'b';
			newfile[len+2] = 'i';
			newfile[len+3] = 'n';
			newfile[len+4] = '\0';
		}
		
		std::size_t floatCount;
		unsigned int indiceCount;
		std::vector<float> buffer;
		mesh.getBufferData(buffer, floatCount, indiceCount);
		saveBinaryData(newfile, buffer.data(), sizeof(float), floatCount);
		printf("%s converted to %s\n", argv[2], newfile);
	}
	
	return 0;
}

void saveOrigo(){
	/* Simple 2d origin */
	float w = 0.075f;
	float d = 2.0f;
	float a = 0.15f;
	float p = 2.3f;
	float data[] = {
		w,0,w, -w,0,-w, -w,0,d,
		w,0,w, -w,0,d, w,0,d,
		a,0,d, -a,0,d, 0,0,p,

		w,0,w, d,0,-w, -w,0,-w,
		w,0,w, d,0,w, d,0,-w,
		d,0,-a, d,0,a, p,0,0,

		w,0,w, -w,d,-w, -w,0,-w,
		w,0,w, w,d,w, -w,d,-w,
		a,d,a, 0,p,0, -a,d,-a
	};
	saveBinaryData("origo.bin", data, sizeof(float), 81);
}