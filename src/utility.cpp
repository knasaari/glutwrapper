#include "utility.h"
#include <cmath>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>

float degToRad(float degrees){
	return degrees * PI / 180.0f;
}

float radToDeg(float radians){
	return radians * 180.0f / PI;	
}

Vec4f eulerToQuaternion(const Vec3f& euler){
	float c1 = std::cos(euler.y()/2.0f);
	float s1 = std::sin(euler.y()/2.0f);
	float c2 = std::cos(euler.z()/2.0f);
	float s2 = std::sin(euler.z()/2.0f);
	float c3 = std::cos(euler.x()/2.0f);
	float s3 = std::sin(euler.x()/2.0f);
	float c1c2 = c1*c2;
	float s1s2 = s1*s2;
	
	Vec4f quaternion(
		c1c2*s3 + s1s2*c3,
		s1*c2*c3 + c1*s2*s3,
		c1*s2*c3 - s1*c2*s3,
		c1c2*c3 - s1s2*s3);
	
	return quaternion;
}

void quaternionToMat4(Matrix4f* matrix, const Vec4f& q){
	Vec4f sq = q*q;
	float sqw = sq.w();
	float sqx = sq.x();
	float sqy = sq.y();
	float sqz = sq.z();

	float invs = 1.0f / (sqx + sqy + sqz + sqw);
	matrix->m00 = ( sqx - sqy - sqz + sqw)*invs;
	matrix->m11 = (-sqx + sqy - sqz + sqw)*invs;
	matrix->m22 = (-sqx - sqy + sqz + sqw)*invs;

	float tmp1 = q.x()*q.y();
	float tmp2 = q.z()*q.w();
	matrix->m01 = 2.0f * (tmp1 + tmp2)*invs;
	matrix->m10 = 2.0f * (tmp1 - tmp2)*invs;

	tmp1 = q.x()*q.z();
	tmp2 = q.y()*q.w();
	matrix->m02 = 2.0f * (tmp1 - tmp2)*invs;
	matrix->m20 = 2.0f * (tmp1 + tmp2)*invs;
	tmp1 = q.y()*q.z();
	tmp2 = q.x()*q.w();
	matrix->m12 = 2.0f * (tmp1 + tmp2)*invs;
	matrix->m21 = 2.0f * (tmp1 - tmp2)*invs;      
}

std::ostream& operator<< (std::ostream& stream, Vec4f v){
	stream << v.x() << "," << v.y() << "," << v.z() << "," << v.w();
	return stream;
}

Vec4f mulQuaternion(Vec4f q1, Vec4f q2) {
	Vec4f xxxx(q1.x(),-q1.x(),q1.x(),-q1.x());
	Vec4f yyyy(q1.y(),q1.y(),-q1.y(),-q1.y());
	Vec4f zzzz(-q1.z(),q1.z(),q1.z(),-q1.z());
	Vec4f wwww(q1.w());
	Vec4f quaternion = (xxxx * q2.wzyx() + yyyy * q2.zwxy() + zzzz * q2.yxwz() + wwww * q2);

//	Vec4f quaternion(q1.x() * q2.w() + q1.y() * q2.z() - q1.z() * q2.y() + q1.w() * q2.x(),
//					-q1.x() * q2.z() + q1.y() * q2.w() + q1.z() * q2.x() + q1.w() * q2.y(),
//					 q1.x() * q2.y() - q1.y() * q2.x() + q1.z() * q2.w() + q1.w() * q2.z(),
//					-q1.x() * q2.x() - q1.y() * q2.y() - q1.z() * q2.z() + q1.w() * q2.w());
	return quaternion;
}

Vec4f axisAngleToQuaternion(const Vec4f& axisAngle){
	Vec4f quaternion;
	float s = std::sin(axisAngle.w()/2.0f);
	quaternion = axisAngle * Vec4f(s,s,s,std::cos(axisAngle.w()/2.0f));
	return quaternion;
}
void mme_transformMat4Vec3(const Matrix4f *left, const float *right, float w, float *dest){
	const __m128 m[4] =  {
		_mm_setr_ps(left->m00,left->m01,left->m02,left->m03),
		_mm_setr_ps(left->m10,left->m11,left->m12,left->m13),
		_mm_setr_ps(left->m20,left->m21,left->m22,left->m23),
		_mm_setr_ps(left->m30,left->m31,left->m32,left->m33)
	};
	__m128 xxxx = _mm_set1_ps(right[0]);
	__m128 yyyy = _mm_set1_ps(right[1]);
	__m128 zzzz = _mm_set1_ps(right[2]);
	__m128 wwww = _mm_set1_ps(w);
	_mm_storeu_ps(dest,
	_mm_add_ps(
		_mm_add_ps(
			_mm_mul_ps(m[0], xxxx),
			_mm_mul_ps(m[1], yyyy)
		),
		_mm_add_ps(
			_mm_mul_ps(m[2], zzzz),
			_mm_mul_ps(m[3], wwww)
		)
	));
}

void saveBinaryData(const char* filename, const void* data, unsigned int size, unsigned int count){
	FILE* file;
	file = fopen(filename, "wb");
	if(file != NULL){
		fwrite(data, size, count, file);
		fclose(file);
	}
}

void saveCharData(const char* filename, const void* data, unsigned int size, unsigned int count){
	std::ostringstream ss;
	float *p = (float*)data;
	for(unsigned int i=0;i<count;i++){
		ss << *p++ << "\n";
	}
	
	std::ofstream os(filename, std::ios::binary | std::ios::out);
	if (os.is_open()){
		os << ss.str();
		os.close();
	}
}

std::string readFileToString(const char* filename) {
	std::ostringstream oss;
	std::string s;

	if (oss << std::ifstream(filename).rdbuf()) {
		s = oss.str();
	} else {
		std::cout << "Failed opening " << filename << "\n";
		throw 1;
	}
	
	return s;
}

std::vector<std::string> readFile(const char* filename) {
	std::vector<std::string> file;
	std::ifstream stream(filename);
	std::string buffer;
	if(stream.is_open()){
		while(std::getline(stream, buffer)){
			file.push_back(buffer);
		}
		stream.close();
	}
	else{
		std::cout << "Failed opening " << filename << "\n";
		throw 1;
	}
	
	return file;
}

/**
 * Copyright (c) 2005-2016 Lode Vandevenne
 * 
 * @param buffer output
 * @param filename file to load
 */
void loadFile(std::vector<unsigned char>& buffer, const std::string& filename) {
	std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if(!file.is_open()){
		std::cout << "Failed opening " << filename << "\n";
		throw 1;
	}
	
	//get filesize
	std::streamsize size = 0;
	if (file.seekg(0, std::ios::end).good()) size = file.tellg();
	if (file.seekg(0, std::ios::beg).good()) size -= file.tellg();

	//read contents of the file into the vector
	if (size > 0) {
		buffer.resize((size_t) size);
		file.read((char*) (&buffer[0]), size);
	} else buffer.clear();
}
