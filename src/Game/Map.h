#ifndef MAP_H
#define MAP_H

#include <map>
#include "../Engine/Entity/Entity.h"
#include "../Engine/Mesh/Mesh.h"
#include "../Engine/Mesh/MeshStitcher.h"

class Map : public Entity{
private:
	
enum Track : unsigned int {
	S_NS		= 0,
	S_EW		= 1,
	CURVE_EN	= 2,
	CURVE_NW	= 3,
	CURVE_WS	= 4,
	CURVE_SE	= 5,
	CROSSX		= 6,
	CROSST_ENW	= 7,
	CROSST_NWS	= 8,
	CROSST_WSE	= 9,
	CROSST_SEN	= 10,
	COUNT		= 11,
	EMPTY		= 12
};

enum Directions : unsigned int {
	E		= 0x1,
	NE		= 0x2,
	N		= 0x4,
	NW		= 0x8,
	W		= 0x10,
	SW		= 0x20,
	S		= 0x40,
	SE		= 0x80
};

public:	
	const static unsigned int mapSize = 70;
	Map();
	virtual ~Map();
	void generate();
private:
	unsigned int map[mapSize][mapSize];
        std::map<unsigned int, Mesh> meshes;
        Mesh unknown;
        
	bool inBounds(unsigned int x, unsigned int y);
	unsigned int scan(unsigned int x, unsigned int y);
	unsigned int getTile(unsigned int x, unsigned int y);
	
};

#endif /* MAP_H */

