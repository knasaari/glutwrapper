#include "Map.h"
#include "matrix4.h"
#include "../Engine/Mesh/VBO.h"
#include <random>
#include <chrono>
Map::Map() {
    Entity rotationHelper;
    meshes[S].loadObj("../assets/track/track_short.obj");
    meshes[E] = meshes[S];
    meshes[N] = meshes[S];
    meshes[W] = meshes[S];

    meshes[N|S|W|E].loadObj("../assets/track/track_x_cross.obj");
    unknown = meshes[N|S|W|E];

    meshes[S|E|N].loadObj("../assets/track/track_t_cross.obj");
    meshes[E|N|W] = meshes[S|E|N];
    meshes[N|W|S] = meshes[S|E|N];
    meshes[W|S|E] = meshes[S|E|N];

    meshes[N|S].loadObj("../assets/track/track.obj");
    meshes[E|W] = meshes[N|S];

    meshes[S|E].loadObj("../assets/track/track_90_corner.obj");
    meshes[N|E] = meshes[S|E];
    meshes[N|W] = meshes[S|E];
    meshes[S|W] = meshes[S|E];

    rotationHelper.setRotation(Vec3f(0,PI/2,0)); // +90
    meshes[E|W].transform(rotationHelper.getMatrix());
    meshes[E|N|W].transform(rotationHelper.getMatrix());
    meshes[N|E].transform(rotationHelper.getMatrix());
    meshes[E].transform(rotationHelper.getMatrix());

    rotationHelper.setRotation(Vec3f(0,PI,0)); // +180
    meshes[N|W|S].transform(rotationHelper.getMatrix());
    meshes[N|W].transform(rotationHelper.getMatrix());
    meshes[N].transform(rotationHelper.getMatrix());

    rotationHelper.setRotation(Vec3f(0,PI/2*3,0)); // +270
    meshes[W|S|E].transform(rotationHelper.getMatrix());
    meshes[S|W].transform(rotationHelper.getMatrix());
    meshes[W].transform(rotationHelper.getMatrix());

    rotationHelper.setScale(Vec3f(0.5f));
    unknown.transform(rotationHelper.getMatrix());
}

Map::~Map() {
}

void Map::generate(){
	std::chrono::high_resolution_clock::time_point preLoad = std::chrono::high_resolution_clock::now();
	
	Entity helper;
	Matrix4f transmat = *helper.getMatrix();
	uint64_t seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	std::seed_seq seeder({uint32_t(seed),uint32_t(seed >> 32)});
	
	std::default_random_engine gen(seeder);
	std::uniform_int_distribution<unsigned int> distrib(0,2);
	
	MeshStitcher stitcher;
	for(unsigned int y=0;y<mapSize;y++){
		for(unsigned int x=0;x<mapSize;x++){
			//map[y][x] = (distrib(gen)>0?1:0);
			map[y][x] = 1;
		}
	}
	
	unsigned int mask = E | N | W | S;
	int pieceCount = 0;
	for(unsigned int y=0;y<mapSize;y++){
		for(unsigned int x=0;x<mapSize;x++){
			if(getTile(x,y) != EMPTY){
				unsigned int s = scan(x,y) & mask;

				std::map<unsigned int, Mesh>::iterator it = meshes.find(s);
				
				if(it != meshes.end()){
					pieceCount++;
					transmat.m30 = x;
					transmat.m32 = y;
					stitcher.add(it->second, &transmat);
				}
				else{
					// A single piece of track not connected to anything
					map[y][x] = 0;
				}
			}
		}
	}
	
	const float* tdata;
	unsigned int tInds;
	std::size_t tSize;
	tdata = stitcher.getBuffer(tSize, tInds);
	
	//saveCharData("mapdump.txt",tdata.data(),sizeof(float),tSize);
	
	if(!model.vbo){
		std::shared_ptr<VBO> tracks = std::make_shared<VBO>();
		tracks->create(tdata, tSize, tInds, unknown.getCollection());
		
		model.vbo = tracks;
	}
	else
		model.vbo->update(tdata, tSize, tInds);
	
	std::chrono::high_resolution_clock::time_point afterLoad = std::chrono::high_resolution_clock::now();
	std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(afterLoad - preLoad);
	std::cout << tSize << " pieces: " << pieceCount << ", generating took: " << ms.count() << "ms" << std::endl;
}

bool Map::inBounds(unsigned int x, unsigned int y){
	return x >= 0 && x < mapSize && y >= 0 && y < mapSize;
}

unsigned int Map::getTile(unsigned int x, unsigned int y){
    if(inBounds(x,y) && map[y][x] != 0){
	return map[y][x];
    }
    
    return EMPTY;
}

unsigned int Map::scan(unsigned int x, unsigned int y){
	unsigned int result = 0;
	
	if(getTile(x-1,y-1) != EMPTY){
		result |= NW;
	}
	if(getTile(x,y-1) != EMPTY){
		result |= N;
	}
	if(getTile(x+1,y-1) != EMPTY){
		result |= NE;
	}
	
	if(getTile(x-1,y) != EMPTY){
		result |= W;
	}
	if(getTile(x+1,y) != EMPTY){
		result |= E;
	}
	
	if(getTile(x-1,y+1) != EMPTY){
		result |= SW;
	}
	if(getTile(x,y+1) != EMPTY){
		result |= S;
	}
	if(getTile(x+1,y+1) != EMPTY){
		result |= SE;
	}
	
	return result;
}
