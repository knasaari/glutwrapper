#include "glfwWrapper.h"

using HRClock = std::chrono::high_resolution_clock;
	
namespace glfwWrapper {
	glfwWrapper *glfwWrapper::instance = NULL;
	
	glfwWrapper::glfwWrapper() {
		title = "glfwWrapper";
		
		keyStates = new bool[GLFW_KEY_LAST];
		keyReleaseStates = new bool[GLFW_KEY_LAST];
		appSpeedD = 1.0f;
		tick = 0;
		displayTime = 0;
		framesSinceLastTick = 0;
		
		for(int i=0;i<GLFW_KEY_LAST;i++){
			keyStates[i] = false;
			keyReleaseStates[i] = false;
		}
		for(int i=0;i<3;i++){
			mouseDown[i] = false;
			mouseUp[i] = false;
		}
	}
	
	glfwWrapper::~glfwWrapper() {
	}

	void glfwWrapper::start(int argc, char* argv[]){
		setInstance();
		
		glfwSetErrorCallback(errorCallback);
		
		if (!glfwInit())
			exit(EXIT_FAILURE);
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
		
		window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, title.c_str(), NULL, NULL);
		//glfwSetWindowPos(window, WINDOW_X_POSITION, WINDOW_Y_POSITION);
		
		if (!window){
			glfwTerminate();
			exit(EXIT_FAILURE);
		}
		
		glfwMakeContextCurrent(window);
		
		gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
		
		#ifdef MINGW
		//glfwSwapInterval(1);
		#endif

		glfwSetKeyCallback(window, keyWrapper);
		glfwSetMouseButtonCallback(window, mouseWrapper);
		glfwSetCursorPosCallback(window, mouseMotionWrapper);
		glfwSetFramebufferSizeCallback(window, reshapeWrapper);

		const GLubyte* vendor = glGetString(GL_VENDOR);
		const GLubyte* renderer = glGetString(GL_RENDERER);
		const GLubyte* version = glGetString(GL_VERSION);
		const GLubyte* glsl = glGetString(GL_SHADING_LANGUAGE_VERSION);
		
		std::cout << vendor << " - " << renderer <<
				"\nOpenGL " << version << ", GLSL " << glsl << "\n\n";
		
		init();
		currentTime = HRClock::now();
		while (!glfwWindowShouldClose(window)){
			instance->displayWrapper();
		}
		
		glfwDestroyWindow(window);
		glfwTerminate();
	}
	
	void glfwWrapper::stop(){
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	
	void glfwWrapper::init(){
		glClearColor(0.0, 0.0, 0.0, 1.0);
		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable (GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CCW);
		
		HRClock::time_point preLoad = HRClock::now();
		load();
		HRClock::time_point afterLoad = HRClock::now();
		
		std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(afterLoad - preLoad);
		std::cout << "Loading took " << ms.count() << "ms" << std::endl;
	}
	
	void glfwWrapper::load(){
		//Override
	}
	
	void glfwWrapper::update() {
		//Override
	}
	
	void glfwWrapper::display() {
		//Override
	}
	
	void glfwWrapper::reshape(int width, int height) {
		//Override
	}
	
	void glfwWrapper::setKeyDown(unsigned int key){
		keyReleaseStates[key] = (keyStates[key] == false)?true:false;
		keyStates[key] = true;
	}
	
	void glfwWrapper::setKeyRelease(unsigned int key){
		keyStates[key] = false;
	}
	
	void glfwWrapper::mouseMotion(double x, double y){
		mouseNow = Vec2f(x,y);
	}
	
	void glfwWrapper::mousePassiveMotion(double x, double y){
		mouseNow = Vec2f(x,y);
	}
	
	void glfwWrapper::mouse(int button, int state){
		if(button == GLFW_MOUSE_BUTTON_LEFT){
			if(state == GLFW_PRESS){
				instance->mouseUp[0] = (instance->mouseDown[0] == false)?true:false;
				instance->mouseDown[0] = true;
			}
			else{
				instance->mouseDown[0] = false;
			}
		}
		else if(button == GLFW_MOUSE_BUTTON_RIGHT){
			if(state == GLFW_PRESS){
				instance->mouseUp[2] = (instance->mouseDown[2] == false)?true:false;
				instance->mouseDown[2] = true;
			}
			else{
				instance->mouseDown[2] = false;
			}
		}
	}
	
	void glfwWrapper::setInstance(){
		instance = this;
	}
	
	
	float glfwWrapper::appSpeed(){
		return (float)this->appSpeedD;
	}
	
	double glfwWrapper::appSpeedDouble(){
		return this->appSpeedD;
	}
	
	bool glfwWrapper::keyHit(unsigned int key){
		return keyReleaseStates[key];
	}
	
	bool glfwWrapper::keyDown(unsigned int key){
		return keyStates[key];
	}
	
	Vec2f glfwWrapper::mousePos(){
		return mouseNow;
	}
	
	Vec2f glfwWrapper::mouseDelta(){
		return mouseDeltaPos;
	}
	
	/* WRAPPERS */
	
	void glfwWrapper::displayWrapper() {
		glfwWrapper::updateWrapper();
		
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		
		HRClock::time_point preDraw = HRClock::now();
		instance->display();
		glfwSwapBuffers(instance->window);
		HRClock::time_point afterDraw = HRClock::now();
		std::chrono::microseconds ms = std::chrono::duration_cast<std::chrono::microseconds>(afterDraw - preDraw);
		instance->displayTime = ms.count();
		
		glfwPollEvents();
	}
	
	void glfwWrapper::updateWrapper() {
		instance->lastTime = instance->currentTime;
		instance->currentTime = HRClock::now();
		double seconds = std::chrono::duration_cast<std::chrono::duration<double>>(instance->currentTime - instance->lastTime).count();
		instance->appSpeedD = 60.0 / (1.0 / seconds);
		
		instance->tick += instance->appSpeed();
		instance->framesSinceLastTick++;
		if(instance->tick > 60){
			instance->tick = instance->tick-60.0f;
			std::cout << "FPS: " << instance->framesSinceLastTick << std::endl;
			instance->framesSinceLastTick = 0;
		}
		instance->mouseDeltaPos = instance->mouseNow - instance->mouseLast;
		instance->update();
		instance->mouseLast = instance->mouseNow;
		
		for(int i=0;i<GLFW_KEY_LAST;i++){
			instance->keyReleaseStates[i] = false;
		}
	}
	
	void glfwWrapper::reshapeWrapper(GLFWwindow* window, int width, int height) {
		glViewport(0,0,(GLsizei)width,(GLsizei)height);
		instance->reshape(width,height);
	}
	
	void glfwWrapper::keyWrapper(GLFWwindow* window, int key, int scancode, int action, int mods){
		unsigned int keyCode = key;
		if(key != GLFW_KEY_UNKNOWN){
			
			if(action == GLFW_PRESS){
				instance->setKeyDown(keyCode);
			}
			else if(action == GLFW_RELEASE){
				instance->setKeyRelease(keyCode);
			}
		}
	}
	
	void glfwWrapper::mouseMotionWrapper(GLFWwindow* window, double x, double y){
		instance->mouseMotion(x,y);
	}
	
	void glfwWrapper::mousePassiveMotionWrapper(int x, int y){
		instance->mousePassiveMotion(x,y);
	}
	
	void glfwWrapper::mouseWrapper(GLFWwindow* window, int button, int action, int mods){
		instance->mouse(button,action);
	}
	
	void glfwWrapper::errorCallback(int error, const char* description){
		std::cerr <<  "Error: " << description << std::endl;
	}
}
