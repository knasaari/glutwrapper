#ifndef GLFWWRAPPER_H
#define GLFWWRAPPER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <cmath>
#include <chrono>
#include "vec.h"

namespace glfwWrapper {
	class glfwWrapper {
	private:
		bool* keyStates;
		bool* keyReleaseStates;
		Vec2f mouseNow, mouseLast, mouseDeltaPos;
		std::chrono::high_resolution_clock::time_point lastTime, currentTime;
		float appSpeedD, tick;
		unsigned int framesSinceLastTick;
		long long int displayTime;
	protected:
		std::string title;
		bool mouseDown[3];
		bool mouseUp[3];
		static glfwWrapper *instance;
		GLFWwindow* window;
	public:
		const static int WINDOW_WIDTH = 1280;
		const static int WINDOW_HEIGHT = 800;
		const static int WINDOW_X_POSITION = 100;
		const static int WINDOW_Y_POSITION = 100;

		glfwWrapper();
		virtual ~glfwWrapper();
		void start(int argc, char *argv[]);
		void stop();
		virtual void update();
		virtual void display();
		virtual void reshape(int width, int height);
		virtual void setKeyDown(unsigned int key);
		virtual void setKeyRelease(unsigned int key);
		virtual void mouseMotion(double x, double y);
		virtual void mousePassiveMotion(double x, double y);
		virtual void mouse(int button, int state);
		virtual void load();
		void setInstance(); 
		void init();
		float appSpeed();
		double appSpeedDouble();
		bool keyHit(unsigned int key);
		bool keyDown(unsigned int key);
		Vec2f mousePos();
		Vec2f mouseDelta();
		
		static void displayWrapper();
		static void updateWrapper();
		static void reshapeWrapper(GLFWwindow* window, int width, int height);
		static void keyWrapper(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void mouseMotionWrapper(GLFWwindow* window, double x, double y);
		static void mousePassiveMotionWrapper(int x, int y);
		static void mouseWrapper(GLFWwindow* window, int button, int action, int mods);
		static void errorCallback(int error, const char* description);
	};
}

#endif /* GLFWWRAPPER_H */

