#ifndef FRUSTUM_H
#define FRUSTUM_H

#include "matrix4.h"
#include <vec.h>

class Frustum {
public:
	Frustum();
	virtual ~Frustum();
	void update(const Matrix4f &projectionMatrix, const Matrix4f &viewMatrix, bool normalize);
	bool pointInFrustum(const Vec3f &point);
	bool sphereInFrustum(const Vec3f &point, float radius);
	float distanceToPoint(const Vec4f &plane, const Vec3f &point);
	Vec4f plane[6];
};

#endif /* FRUSTUM_H */

