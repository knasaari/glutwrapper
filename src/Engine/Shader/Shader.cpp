#include <glad/glad.h>

#include "Shader.h"

Shader::Shader(const char* vertPath, const char* fragPath){
	ready = false;
	this->vertPath = vertPath;
	this->fragPath = fragPath;
	
}

Shader::~Shader(){
	this->deleteShaderObjects();
	this->deleteProgram();
	std::cout << "Shader " << this->vertPath << " " << this->fragPath << " freed.\n";
}

bool Shader::createProgram(){
	this->vertShader = this->createShader(GL_VERTEX_SHADER, this->vertPath);
	this->fragShader = this->createShader(GL_FRAGMENT_SHADER, this->fragPath);

	if(this->vertShader == 0 || this->fragShader == 0) {
		return false;
	}
	
	this->program = glCreateProgram();
	
	if(this->program == 0) {
		return false;
	}
	
	glAttachShader(this->program, this->vertShader);
	glAttachShader(this->program, this->fragShader);
	
	/* Set Attributes */
	this->setAttributes();
	
	glLinkProgram(this->program);
	
	GLint linkStatus;
	glGetProgramiv(this->program, GL_LINK_STATUS, &linkStatus);
	if(linkStatus == GL_FALSE){
		std::cout << "Shader program link status false.\n";
		this->printProgramLog(this->program);
		return false;
	}

	glValidateProgram(this->program);
	this->printProgramLog(this->program);
	
	this->preSetUniforms();
	glUseProgram(this->program);

	/* Set Uniforms */
	this->setUniforms();
	
	glUseProgram(0);
	
	std::cout << "Loaded " << this->vertPath << " & " << this->fragPath << "\n";
	return true;
}

void Shader::setAttributes(){
	glBindAttribLocation(this->program, 0, "OBJ_Position");
	glBindAttribLocation(this->program, 1, "OBJ_Texcoord");
	glBindAttribLocation(this->program, 2, "OBJ_Normal");
	glBindAttribLocation(this->program, 3, "OBJ_Tangent");
	glBindAttribLocation(this->program, 4, "OBJ_Bitangent");
	glBindAttribLocation(this->program, 5, "InstanceMatrix");
	
	glBindFragDataLocation(program, 0, "FragColor");
}

void Shader::setUniforms(){
	//Overload this
	
	int location;
	location = glGetUniformLocation(this->program, "Difftexture0");
	glUniform1i(location, 0);
	location = glGetUniformLocation(this->program, "Difftexture1");
	glUniform1i(location, 1);
	location = glGetUniformLocation(this->program, "Difftexture2");
	glUniform1i(location, 2);
	location = glGetUniformLocation(this->program, "Difftexture3");
	glUniform1i(location, 3);
	
	this->viewPos = glGetUniformLocation(this->program, "viewPos");
	glUniform3f(this->viewPos, 0, 0, 0);
	this->lightPos_location = glGetUniformLocation(this->program, "lightPos");
	glUniform4f(this->lightPos_location, 0, 0, 0, 0);
	
	Matrix4f mat;
	setIdentityMat4(&mat);
	this->modelMatrix = glGetUniformLocation(this->program, "ModelMatrix");
	glUniformMatrix4fv(this->modelMatrix, 1, false, mat.m);
	this->viewMatrix = glGetUniformLocation(this->program, "ViewMatrix");
	glUniformMatrix4fv(this->viewMatrix, 1, false, mat.m);
	this->projectionMatrix = glGetUniformLocation(this->program, "ProjectionMatrix");
	glUniformMatrix4fv(this->projectionMatrix, 1, false, mat.m);
	this->normalMatrix = glGetUniformLocation(this->program, "NormalMatrix");
	glUniformMatrix4fv(this->normalMatrix, 1, false, mat.m);
	this->modelNormalMatrix = glGetUniformLocation(this->program, "ModelNormalMatrix");
	glUniformMatrix4fv(this->modelNormalMatrix, 1, false, mat.m);
}

void Shader::preSetUniforms(){
}

void Shader::deleteProgram(){
	if(glIsProgram(this->program))
		glDeleteProgram(this->program);
}

void Shader::deleteShaderObjects(){
	if(glIsShader(this->vertShader)){
		glDetachShader(this->program, this->vertShader);
		glDeleteShader(this->vertShader);
	}
	
	if(glIsShader(this->fragShader)){
		glDetachShader(this->program, this->fragShader);
		glDeleteShader(this->fragShader);
	}
}
/**
 * Creates the Shader from the sources given in constructor.
 * Must be called before anything else is done with the Shader.
 * 
 * @return True is Shader is compiled and ready to be used.
 */
bool Shader::create(){
	ready = this->createProgram();
	deleteShaderObjects();
	if(ready == false){
		deleteProgram();
	}
	return ready;
}

/**
 * Reloads the Shader program from disk
 * 
 * TODO: Check that the Shader has been created before.
 */
void Shader::refresh(){
	deleteProgram();

	create();
}

/**
 * Compiles the shader source.
 * 
 * @param type GLenum GL_VERTEX_SHADER or GL_FRAGMENT_SHADER
 * @param path Source file name
 * @return shader object handle or 0 if failed.
 */
int Shader::createShader(GLenum type, std::string path){
	GLuint shader = 0;
	
	shader = glCreateShader(type);
	
	if(shader == 0) {
		return 0;
	}
	
	std::string shaderCode = readFileToString(path.c_str());

	const char * p = shaderCode.c_str();
	
	glShaderSource(shader, 1, &p, NULL);
	glCompileShader(shader);
	
	GLint shaderStatus;
	glGetShaderiv(shader,GL_COMPILE_STATUS, &shaderStatus);
	
	std::cout << path << " " << shaderStatus << "\n";
	
	if(shaderStatus == 0){
		printShaderLog(shader);
		glDeleteShader(shader);
		return shaderStatus;
	}
	
	return shader;
}

/**
 * Enables the Shader program for rendering.
 */
void Shader::enable(const Matrix4f* viewMatrix, const Matrix4f* projectionMatrix, const Vec3f& cameraPos){
	glUseProgram(this->program);
	glUniformMatrix4fv(this->viewMatrix, 1, false, viewMatrix->m);
	glUniformMatrix4fv(this->projectionMatrix, 1, false, projectionMatrix->m);
	glUniform3f(this->viewPos, cameraPos.x(), cameraPos.y(), cameraPos.z());
	glUniform4f(this->lightPos_location, lightPos.x(), lightPos.y(), lightPos.z(), lightPos.w());
}

void Shader::disable(){
	glUseProgram(0);
}

bool Shader::isReady(){
	return ready;
}

void Shader::setModelMatrix(const Matrix4f* modelMatrix){
	glUniformMatrix4fv(this->modelMatrix, 1, false, modelMatrix->m);
}

void Shader::setNormalMatrix(const Matrix4f* viewMatrix, const Matrix4f* modelMatrix){
	Matrix4f normalMatrix, modelNormalMatrix;
	
	mulMat4(viewMatrix, modelMatrix, &normalMatrix);
	invertMat4(&normalMatrix);
	transposeMat4(&normalMatrix);
	
	modelNormalMatrix = *modelMatrix;
	invertMat4(&modelNormalMatrix);
	transposeMat4(&modelNormalMatrix);
	
	glUniformMatrix4fv(this->normalMatrix, 1, false, normalMatrix.m);
	glUniformMatrix4fv(this->modelNormalMatrix, 1, false, modelNormalMatrix.m);
}

void Shader::setLightPos(Vec4f lightPos){
	lightPos.m = lightPos.m;
}

/**
 * Checks the shader log for errors and outputs them if found.
 * 
 * @param obj vertex/fragment shader or whole shader program.
 */
void Shader::printProgramLog(GLuint obj){
	int infologLength = 0;
	int maxLength = 0;

	glGetProgramiv(obj,GL_INFO_LOG_LENGTH,&maxLength);

	char infoLog[maxLength];
	
	glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
	
	if (infologLength > 0){
		FILE * pFile = fopen("glsl_log.txt","w");
		if(pFile != NULL){
			fputs(infoLog,pFile);
			fclose(pFile);
		}
		printf("\n%s\n",infoLog);
	}
}

void Shader::printShaderLog(GLuint shader){
	int infologLength = 0;
	int maxLength = 0;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	char infoLog[maxLength];
	
	glGetShaderInfoLog(shader, maxLength, &infologLength, infoLog);
	if (infologLength > 0){
		FILE * pFile = fopen("glsl_log.txt","w");
		if(pFile != NULL){
			fputs(infoLog,pFile);
			fclose(pFile);
		}
		printf("\n%s\n",infoLog);
	}
}

void Shader::drawArrays(unsigned int primitive, int first, int indiceCount){
	glDrawArrays(primitive, first, indiceCount);
}

void Shader::drawArraysInstanced(unsigned int primitive, int first, int indiceCount, int primCount){
	glDrawArraysInstanced(primitive, first, indiceCount, primCount);
}

void Shader::bindArrayBuffer(unsigned int buffer){
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
}

void Shader::setVertexAttributes(const VertexAttributeCollection &vac){
	const std::vector<VertexAttribute> &collection = vac.collection;
	for (int i = 0; i < collection.size(); i++) {
		const VertexAttribute &attribute = collection[i];
		glVertexAttribPointer(attribute.index, attribute.size, GL_FLOAT, attribute.normalized, attribute.stride, (GLvoid*)attribute.offset);
		glEnableVertexAttribArray(attribute.index);
		if(attribute.divisor != 0)
			glVertexAttribDivisor(attribute.index, attribute.divisor);
	}
}

void Shader::unsetVertexAttributes(const VertexAttributeCollection &vac){
	const std::vector<VertexAttribute> &collection = vac.collection;
	for (int i = 0; i < collection.size(); i++) {
		const VertexAttribute &attribute = collection[i];
		if(attribute.divisor != 0)
			glVertexAttribDivisor(attribute.index, 0);
		glDisableVertexAttribArray(attribute.index);
	}
}

void Shader::enableVertexAttribute(unsigned int index){
	glEnableVertexAttribArray(index);
}

void Shader::disableVertexAttribute(unsigned int index){
	glDisableVertexAttribArray(index);
}

void Shader::vertexAttribPointer(const VertexAttribute& attribute){
	glVertexAttribPointer(attribute.index, attribute.size, GL_FLOAT, attribute.normalized, attribute.stride, (GLvoid*)attribute.offset);
}

void Shader::vertexAttribPointer(unsigned int index, int size, unsigned int type, bool normalized, int stride, int offset){
	glVertexAttribPointer(index, size, GL_FLOAT, normalized, stride, (GLvoid*)offset);
}

void Shader::vertexAttribDivisor(unsigned int index, unsigned int divisor){
	glVertexAttribDivisor(index, divisor);
}

void Shader::bindTexture(unsigned int index, unsigned int handle, unsigned int target){
	glActiveTexture(GL_TEXTURE0+index);
	glBindTexture(target, handle);
}

void Shader::bindTexture(unsigned int index, const Texture& texture){
	glActiveTexture(GL_TEXTURE0+index);
	glBindTexture(texture.getTarget(), texture.getHandle());
}

void Shader::bindTexture(unsigned int index, const Texture2D& texture){
	glActiveTexture(GL_TEXTURE0+index);
	glBindTexture(GL_TEXTURE_2D, texture.getHandle());
}

void Shader::bindTexture(unsigned int index, const TextureCube& texture){
	glActiveTexture(GL_TEXTURE0+index);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture.getHandle());
}

