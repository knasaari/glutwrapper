#ifndef SHADER_H
#define	SHADER_H

#include <stdlib.h>
#include <glad/glad.h>
#include <string>
#include <iostream>
#include <vector>
#include "matrix4.h"
#include "../../utility.h"
#include "../Mesh/VertexAttribute.h"
#include "../Texture/Texture.h"
#include "../Texture/Texture2D.h"
#include "../Texture/TextureCube.h"

class Shader : public VecfAlign{
private:
	GLuint vertShader, fragShader;
	std::string vertPath, fragPath;
	GLuint viewMatrix, modelMatrix, projectionMatrix, normalMatrix, modelNormalMatrix, viewPos, lightPos_location;
	Vec4f lightPos;
	void printProgramLog(GLuint obj);
	void printShaderLog(GLuint shader);
	int createShader(GLenum type, std::string path);
	bool createProgram();
	void deleteProgram();
	void deleteShaderObjects();
	
protected:
	GLuint program;
	bool ready;
	virtual void setAttributes();
	virtual void setUniforms();
	virtual void preSetUniforms();
 
public:
	Shader(const char * vertPath, const char * fragPath);
	virtual ~Shader();

	bool create();
	void refresh();
	void enable(const Matrix4f* viewMatrix, const Matrix4f* projectionMatrix, const Vec3f& cameraPos);
	void disable();
	void setModelMatrix(const Matrix4f* modelMatrix);
	void setNormalMatrix(const Matrix4f* viewMatrix, const Matrix4f* modelMatrix);
	void setLightPos(Vec4f lightPos);
	bool isReady();
	void drawArrays(unsigned int primitive, int first, int indiceCount);
	void drawArraysInstanced(unsigned int primitive, int first, int indiceCount, int primCount);
	void bindArrayBuffer(unsigned int buffer);
	void setVertexAttributes(const VertexAttributeCollection &vac);
	void unsetVertexAttributes(const VertexAttributeCollection &vac);
	void enableVertexAttribute(unsigned int index);
	void disableVertexAttribute(unsigned int index);
	void vertexAttribPointer(const VertexAttribute& attribute);
	void vertexAttribPointer(unsigned int index, int size, unsigned int type, bool normalized, int stride, int offset);
	void vertexAttribDivisor(unsigned int index, unsigned int divisor);
	void bindTexture(unsigned int index, unsigned int handle, unsigned int target = GL_TEXTURE_2D);
	void bindTexture(unsigned int index, const Texture& texture);
	void bindTexture(unsigned int index, const Texture2D& texture);
	void bindTexture(unsigned int index, const TextureCube& texture);
	//void bindTexture(unsigned int index, Texture);
};
#endif	/* SHADER_H */
