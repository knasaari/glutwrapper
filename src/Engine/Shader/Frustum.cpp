#include "Frustum.h"

Frustum::Frustum() {
}

Frustum::~Frustum() {
}

void Frustum::update(const Matrix4f& projectionMatrix, const Matrix4f& viewMatrix, bool norm) {
	Matrix4f comboMatrix;
	mulMat4(&projectionMatrix, &viewMatrix, &comboMatrix);
	

	plane[0] = Vec4f(comboMatrix.m03 + comboMatrix.m00,
		comboMatrix.m13 + comboMatrix.m10,
		comboMatrix.m23 + comboMatrix.m20,
		comboMatrix.m33 + comboMatrix.m30);
	// Right clipping plane
	plane[1] = Vec4f(comboMatrix.m03 - comboMatrix.m00,
		comboMatrix.m13 - comboMatrix.m10,
		comboMatrix.m23 - comboMatrix.m20,
		comboMatrix.m33 - comboMatrix.m30);
	// Top clipping plane
	plane[2] = Vec4f(comboMatrix.m03 - comboMatrix.m01,
		comboMatrix.m13 - comboMatrix.m11,
		comboMatrix.m23 - comboMatrix.m21,
		comboMatrix.m33 - comboMatrix.m31);
	// Bottom clipping plane
	plane[3] = Vec4f(comboMatrix.m03 + comboMatrix.m01,
		comboMatrix.m13 + comboMatrix.m11,
		comboMatrix.m23 + comboMatrix.m21,
		comboMatrix.m33 + comboMatrix.m31);
	// Near clipping plane
	plane[4] = Vec4f(comboMatrix.m02,
		comboMatrix.m12,
		comboMatrix.m22,
		comboMatrix.m32);
	// Far clipping plane
	plane[5] = Vec4f(comboMatrix.m03 - comboMatrix.m02,
		comboMatrix.m13 - comboMatrix.m12,
		comboMatrix.m23 - comboMatrix.m22,
		comboMatrix.m33 - comboMatrix.m32);
	
	// Normalize the plane equations, if requested
	if (norm == true) {
		plane[0] = normalize(plane[0]);
		plane[1] = normalize(plane[1]);
		plane[2] = normalize(plane[2]);
		plane[3] = normalize(plane[3]);
		plane[4] = normalize(plane[4]);
		plane[5] = normalize(plane[5]);
	}
}

bool Frustum::pointInFrustum(const Vec3f& point){
	for(unsigned int i=0;i<6;i++){
		if (distanceToPoint(plane[i], point) <= 0) {
				return false;
			}
	}
	return true;
}

bool Frustum::sphereInFrustum(const Vec3f& point, float radius){
	for(unsigned int i=0;i<6;i++){
		float distance = distanceToPoint(plane[i], point);
		if (distance < -radius) {
				return false;
		}
	}
	return true;
}

float Frustum::distanceToPoint(const Vec4f& plane, const Vec3f& point){
	Vec4f point4(point.x(),point.y(),point.z(),1.0f);
	
	return dot(point4, plane);
}
