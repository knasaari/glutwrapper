#ifndef ENGINE_H
#define	ENGINE_H

#include <memory>
#include "../glfwWrapper/glfwWrapper.h"
#include "Entity/Camera.h"
#include "Mesh/Mesh.h"
#include "Mesh/Model.h"
#include "Texture/Texture.h"
#include "Mesh/InstancedBatch.h"
#include "../Game/Map.h"

class Engine : public glfwWrapper::glfwWrapper {
	private:
		PerspectiveCamera camera;
		OrthogonalCamera uiCamera;
		Entity box[10000], lightbulb, ui, skybox;
		Map map;
		Vec4f lightPos;
		InstancedBatch batch;
		
		int entityCount;
		bool renderToggle;
                
		float phase, tick;

		//GLuint shadowMapTexture;
		//const int shadowMapSize = 512;
		
		Shader* shader, *shaderUi, *shaderSimple, *shaderFlat, *shaderSkybox, *shaderCube, *shaderInstanced;
		int shaderSelect;

                void makePlane(std::vector<float>&, const Vec3f&, const Vec3f&, const Vec2f&, const Vec2f&);
	public:
		Engine();
		virtual void load();
		virtual void update();
		virtual void display();
		virtual void reshape(int width, int height);
};

#endif	/* ENGINE_H */

