#include "Camera.h"
#include <cmath>

Camera::Camera() : Entity(){
	this->zNear = 0.1f;
	this->zFar = 100.0f;
	this->setScale(Vec3f(1.0f));
}

Camera::Camera(float zNear, float zFar) : Entity(){
	this->zNear = zNear;
	this->zFar = zFar;
}

Camera::~Camera() {
}

const Matrix4f* Camera::getViewMatrix() {	
	this->update();
	return &this->viewMatrix;
}

const Matrix4f* Camera::getProjectionMatrix() {	
	this->updateProjectionMatrix();
	return &this->projectionMatrix;
}

void Camera::moveLocal(const Vec3f& delta){
	Matrix4f mat;
	setIdentityMat4(&mat);
	quaternionToMat4(&mat, this->quaternion);
	invertMat4(&mat);
	Vec4<float> dest;
	transformMat4(&mat, Vec4<float>(delta.x(), delta.y(), delta.z(), 0).v, dest.v);
	this->position += Vec3f(dest.v);
}

void Camera::update(){
	Entity::update();
	
	setIdentityMat4(&this->viewMatrix);
	quaternionToMat4(&this->viewMatrix, this->quaternion);
	translateMat4(&this->viewMatrix, -this->position.x(), -this->position.y(), -this->position.z());
	
	frustum.update(projectionMatrix, viewMatrix, true);
}

void Camera::updateProjectionMatrix(){
}

PerspectiveCamera::PerspectiveCamera() : Camera() {
	this->fov = 75.0f;
	this->aspectRatio = 1.5f;
}

PerspectiveCamera::PerspectiveCamera(float zNear, float zFar, float fov, float aspectRatio) : Camera(zNear, zFar) {
	this->fov = fov;
	this->aspectRatio = aspectRatio;
}

PerspectiveCamera::~PerspectiveCamera(){
}

void PerspectiveCamera::set(float zNear, float zFar, float fov, float aspectRatio){
	this->zNear = zNear;
	this->zFar = zFar;
	this->fov = fov;
	this->aspectRatio = aspectRatio;
}

void PerspectiveCamera::updateProjectionMatrix(){
	float xymax = zNear * tan(degToRad(fov * 0.5f));
	float ymin = -xymax;
	float xmin = -xymax;

	float width = xymax - xmin;
	float height = xymax - ymin;

	float depth = zFar - zNear;
	float q = -(zFar + zNear) / depth;
	float qn = -2.0f * (zFar * zNear) / depth;

	float w = 2.0f * (zNear / width);
	w = w / aspectRatio;
	float h = 2.0f * zNear / height;

	setZeroMat4(&projectionMatrix);
	projectionMatrix.m00 = w;
	projectionMatrix.m11 = h;
	projectionMatrix.m22 = q;
	projectionMatrix.m23 = -1.0f;
	projectionMatrix.m32 = qn;
}

OrthogonalCamera::OrthogonalCamera() : Camera() {
	this->left = 0;
	this->right = 1;
	this->bottom = 1;
	this->top = 0;
}

OrthogonalCamera::OrthogonalCamera(float zNear, float zFar, float left, float right, float bottom, float top) : Camera(zNear, zFar) {
	this->left = left;
	this->right = right;
	this->bottom = bottom;
	this->top = top;
}

OrthogonalCamera::~OrthogonalCamera(){
}

void OrthogonalCamera::set(float zNear, float zFar, float left, float right, float bottom, float top){
	this->zNear = zNear;
	this->zFar = zFar;
	this->left = left;
	this->right = right;
	this->bottom = bottom;
	this->top = top;
}

void OrthogonalCamera::updateProjectionMatrix(){
	float width = right - left;
	float height = top - bottom;
	float depth = zFar - zNear;
	
	setZeroMat4(&projectionMatrix);
	projectionMatrix.m00 = 2.0f/width;
	projectionMatrix.m30 = -((right+left)/width);
	projectionMatrix.m11 = 2.0f/height;
	projectionMatrix.m31 = -((top+bottom)/height);
	projectionMatrix.m22 = -2.0f/depth;
	projectionMatrix.m32 = -((zFar+zNear)/depth);
	projectionMatrix.m33 = 1.0f;
}



