#ifndef ENTITY_H
#define	ENTITY_H

#include <memory>
#include "matrix4.h"
#include "../../utility.h"
#include "../Shader/Shader.h"
#include "../Mesh/VBO.h"
#include "../Mesh/Model.h"

class Entity {
public:
	Vec3f position, scale;
	Vec4f quaternion;
	
	Model model;
	
	static std::shared_ptr<VBO> coffeeCup, origo;
	static std::shared_ptr<Texture2D> rgbTexture, cupTexture, cupNormalTexture;
	
	Entity();
	virtual ~Entity();
	
	void move(const Vec3f& delta);
	virtual void moveLocal(const Vec3f& delta);
	void turn(const Vec3f& delta);
	void turnLocal(Vec3f delta);
	void setPosition(const Vec3f& position);
	void setRotation(const Vec3f& rotation);
	void setScale(const Vec3f& scale);
	void makeCoffeeCup();
	void makeOrigo();

	const Matrix4f* getMatrix();
	static void initBinMeshes();
protected:
	virtual void update();
private:
	bool matrixDirty;
	Matrix4f matrix;
};

#endif	/* ENTITY_H */
