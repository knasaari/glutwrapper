#ifndef CAMERA_H
#define	CAMERA_H

#include "Entity.h"
#include "../Shader/Frustum.h"

class Camera : public Entity{
public:
	Frustum frustum;
	
	Camera();
	Camera(float zNear, float zFar);
	virtual ~Camera();
	const Matrix4f* getProjectionMatrix();
	const Matrix4f* getViewMatrix();
	void moveLocal(const Vec3f& delta) override;
protected:
	Matrix4f projectionMatrix, viewMatrix;
	float zNear, zFar;
	
private:
	virtual void update();
	virtual void updateProjectionMatrix();
};

class PerspectiveCamera : public Camera {
public:
	PerspectiveCamera();
	PerspectiveCamera(float zNear, float zFar, float fov, float aspectRatio);
	virtual ~PerspectiveCamera();
	void set(float zNear, float zFar, float fov, float aspectRatio);
private:
	float fov, aspectRatio;
	virtual void updateProjectionMatrix();
};

class OrthogonalCamera : public Camera {
public:
	OrthogonalCamera();
	OrthogonalCamera(float zNear, float zFar, float left, float right, float bottom, float top);
	virtual ~OrthogonalCamera();
	void set(float zNear, float zFar, float left, float right, float bottom, float top);
private:
	float left, right, bottom, top;
	virtual void updateProjectionMatrix();
};

#endif	/* CAMERA_H */
