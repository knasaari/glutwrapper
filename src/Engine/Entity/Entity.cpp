#include "Entity.h"
#include "../Mesh/Mesh.h"
#include <glad/glad.h>

#define origo_index_count 180
#define origo_float_count 2520
#define cup2_index_count 14274
#define cup2_float_count 199836

#ifdef MINGW
extern float binary_cup2_bin_start[];
extern float binary_origo3d_uv_bin_start[];
float* origo_data = binary_origo3d_uv_bin_start;
float* cup_data = binary_cup2_bin_start;
#else
extern float _binary_cup2_bin_start[];
extern float _binary_origo3d_uv_bin_start[];
float* origo_data = _binary_origo3d_uv_bin_start;
float* cup_data = _binary_cup2_bin_start;
#endif

std::shared_ptr<VBO> Entity::coffeeCup(new VBO());
std::shared_ptr<VBO> Entity::origo(new VBO());
std::shared_ptr<Texture2D> Entity::rgbTexture(new Texture2D());
std::shared_ptr<Texture2D> Entity::cupTexture(new Texture2D());
std::shared_ptr<Texture2D> Entity::cupNormalTexture(new Texture2D());

Entity::Entity() {
	quaternion = eulerToQuaternion(Vec3f(0,0,0));
	scale = Vec3f(1);
	matrixDirty = true;
	update();
}

Entity::~Entity() {
}

void Entity::makeCoffeeCup(){
	Model model;
	model.vbo = Entity::coffeeCup;
	model.diffuseMap = Entity::cupTexture;
	model.normalMap = Entity::cupNormalTexture;
	this->model = model;
}

void Entity::makeOrigo(){
	Model model;
	model.vbo = Entity::origo;
	model.diffuseMap = Entity::rgbTexture;
	model.normalMap = Entity::cupNormalTexture;
	this->model = model;
}

void Entity::move(const Vec3f& delta){
	this->matrixDirty = true;
	
	this->position += delta;
}

void Entity::moveLocal(const Vec3f& delta){
	this->matrixDirty = true;
	this->position += rotateVec3f(quaternion, delta);
}

/**
 * Turn Entity around local axis
 * 
 * @param delta
 */
void Entity::turnLocal(Vec3f delta){
	this->matrixDirty = true;
	Vec4f q = eulerToQuaternion(delta);
	this->quaternion = mulQuaternion(this->quaternion, q);
}

/**
 * Turn Entity around global axis
 * 
 * @param delta
 */
void Entity::turn(const Vec3f& delta){
	this->matrixDirty = true;
	Vec4f q = eulerToQuaternion(delta);
	this->quaternion = mulQuaternion(q, this->quaternion);
}

void Entity::setPosition(const Vec3f& position){
	this->matrixDirty = true;
	
	this->position = position;
}

void Entity::setRotation(const Vec3f& rotation){
	this->matrixDirty = true;
	
	this->quaternion = eulerToQuaternion(rotation);
}

void Entity::setScale(const Vec3f& scale){
	this->matrixDirty = true;
	
	this->scale = scale;
}

void Entity::update() {
	if(matrixDirty){
		setIdentityMat4(&this->matrix);
		translateMat4(&this->matrix, this->position.x(), this->position.y(), this->position.z());
		quaternionToMat4(&this->matrix, this->quaternion);
		scaleMat4(&this->matrix, this->scale.x(), this->scale.y(), this->scale.z());
		this->matrixDirty = false;
	}
}

const Matrix4f* Entity::getMatrix(){
	this->update();
	return &this->matrix;
}

void Entity::initBinMeshes(){
	VertexAttributeCollection collection;
	collection.add(3,0); // xyz
	collection.add(2,1); // uv
	collection.add(3,2); // normal
	collection.add(3,3); // tangent
	collection.add(3,4); // bitangent
	
	Entity::origo->create(origo_data, origo_float_count, origo_index_count, collection);
	Entity::coffeeCup->create(cup_data, cup2_float_count, cup2_index_count, collection);
	
	unsigned int textureSize = 16*16*4;
	unsigned char *tex = new unsigned char[textureSize];
	
	for(unsigned int i=0; i<textureSize; i+=4){
		tex[i] = (i<16*6*4)?255:0;
		tex[i+1] = (i>=16*6*4 && i<16*11*4)?255:0;
		tex[i+2] = (i>=16*11*4)?255:0;
		tex[i+3] = 255;
	}
	rgbTexture->makeTexture(tex,16,16,4);
	rgbTexture->setFiltering(false);
	
	for(unsigned int i=0; i<textureSize; i+=4){
		if(i%(16*4)>8 && i<16*4*5){
			tex[i] = 80;
			tex[i+1] = 51;
			tex[i+2] = 28;
		}
		else if(i>16*4*13){
			tex[i] = 150;
			tex[i+1] = 150;
			tex[i+2] = 150;
		}
		else{
			tex[i] = 236;
			tex[i+1] = 236;
			tex[i+2] = 234;
		}
		tex[i+3] = 255;
	}
	cupTexture->makeTexture(tex,16,16,4);
	
	for(unsigned int i=0; i<textureSize; i+=4){
		tex[i] = 128;
		tex[i+1] = 128;
		tex[i+2] = 255;
		tex[i+3] = 255;
	}
	cupNormalTexture->makeTexture(tex,16,16,4);
	delete[] tex;
}