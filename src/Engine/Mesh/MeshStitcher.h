#ifndef MESHSTITCHER_H
#define MESHSTITCHER_H

#include <cstddef>
#include <vector>
#include "Mesh.h"

class MeshStitcher {
public:
	MeshStitcher();
	virtual ~MeshStitcher();
	void add(const Mesh &mesh);
	void add(Mesh mesh, const Matrix4f *transform);
	void add(Mesh mesh, const Vec3f &translate);
	void getBuffer(std::vector<float> &buffer, std::size_t &vboSize, unsigned int &indiceCount);
	const float* getBuffer(std::size_t& vboSize, unsigned int& indiceCount);
private:
	std::vector<float> buffer;
	std::size_t vboSize;
	unsigned int indiceCount;
};

#endif /* MESHSTITCHER_H */

