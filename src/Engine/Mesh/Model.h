#ifndef MODEL_H
#define MODEL_H

#include <memory>
#include "VBO.h"
#include "../Texture/Texture.h"

class Model {
public:
	std::shared_ptr<VBO> vbo;
	std::shared_ptr<Texture> diffuseMap, normalMap, cubeMap;

	Model();
	virtual ~Model();
	
	bool operator()(const Model&, const Model&) const;
	friend bool operator==(const Model&, const Model&);
	friend bool operator<(const Model&, const Model&);
private:

};

#endif /* MODEL_H */

