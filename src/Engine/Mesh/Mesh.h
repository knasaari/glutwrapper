#ifndef MESH_H
#define	MESH_H

#include <cstddef>
#include <vector>
#include <memory>
#include <string>
#include "../../utility.h"
#include "VertexAttribute.h"
#include "matrix4.h"

class Index;

class Mesh {
public:
	Mesh();
	virtual ~Mesh();
	
	void loadObj(const char* filename, bool verbose = false);
	void computeNormals(
		std::vector<Index>& indices,
		const VectorVec3f& vertices,
		VectorVec3f& normals
	) const;
	void computeTangentBasis (
		const std::vector<Index>& indices,
		const VectorVec3f& vertices,
		const std::vector<Vec2f>& uvs,
		const VectorVec3f& normals,
		VectorVec3f& tangents,
		VectorVec3f& bitangents
	) const;
	void free();
	std::string getName();
	bool getBufferData(std::vector<float>& buffer, std::size_t &vboSize, unsigned int &indiceCount) const;
	bool getRawData(std::vector<float>& buffer, std::size_t &vboSize) const;
	VertexAttributeCollection getCollection() const;
	int transform(const Matrix4f *mat);
	void translate(Vec3f translation);
private:
	VectorVec3f vertices,normals,tangents,bitangents;
	std::vector<Vec2f> texcoords;
	std::vector<Index> indices;
	std::string name;
	
	Vec3f parseToVec3f(std::string& line);
	Vec2f parseToVec2f(std::string& line);
};

class Index {
public:
	static const int components = 3;
	union {

		struct {
			int v, vt, vn;
		};
		int a[3];
	};
	Index();
	virtual ~Index();
	
	bool operator<(const Index& that) const;
	bool operator==(const Index& that) const;
private:
};

class Vertex {
	Vertex();
private:
        union {
            struct{
                float x,y,z,u,v,nx,ny,nz;
            };
            float a[8];
        };
};

class Face {
public:
	std::vector<Index> indices;
	Face();
	virtual ~Face();
	void createFromString(const std::string& line);
	void setCorrectIndices(int vertices, int texcoords, int normals);
	int getCorrectIndex(int index, int indexSize);
private:
};

#endif	/* MESH_H */

