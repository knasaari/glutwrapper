#include "VBO.h"
#include <iostream>
#include "glad/glad.h"
#include "Mesh.h"

VBO::VBO() {
	buffer = 0;
	indiceCount = 0;
	ready = false;
	primitiveType = GL_TRIANGLES;
	size = 0;
	usage = GL_STATIC_DRAW;
}

VBO::VBO(bool isDynamic){
	buffer = 0;
	indiceCount = 0;
	ready = false;
	primitiveType = GL_TRIANGLES;
	size = 0;
	usage = isDynamic?GL_DYNAMIC_DRAW:GL_STATIC_DRAW;
}

VBO::~VBO(){
	if(buffer != 0)
		std::cout << "Freeing VBO " << buffer << "\n";
	this->free();
}

void VBO::update(const float* data, std::size_t count, unsigned int indiceCount){
    if(!ready){
		create(data, count, indiceCount, collection);
		return;
    }
	
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	if(count > size){
		std::cout << "VBO " << buffer << ": Old size: " << size << ", new size: " << count << std::endl;
		glBufferData(GL_ARRAY_BUFFER, count*sizeof(float), data, usage);
		size = count;
	}
	else{
		glBufferSubData(GL_ARRAY_BUFFER, 0, count*sizeof(float), data);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
    this->indiceCount = indiceCount;
}

void VBO::create(const float* data, std::size_t count, unsigned int indiceCount, const VertexAttributeCollection& collection){
	this->free();
	
	buffer = genBuffer();
	std::cout << "Created VBO " << buffer << "\n";
	this->indiceCount = indiceCount;
	this->size = count;
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, size*sizeof(float), data, usage);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	this->setVertexAttributeCollection(collection);
	this->ready = true;
}

void VBO::create(const Mesh& mesh){
	unsigned int indiceCount = 0;
	std::size_t vboSize = 0;
	std::vector<float> buffer;
	if(mesh.getBufferData(buffer, vboSize, indiceCount)){
		this->create(buffer.data(), vboSize, indiceCount, mesh.getCollection());
	}
}

void VBO::setVertexAttributeCollection(const VertexAttributeCollection& collection){
	this->collection = collection;
}

void VBO::free(){
	ready = false;
	indiceCount = 0;
	size = 0;
	if(buffer != 0){
		glDeleteBuffers(1, &buffer);
	}
	buffer = 0;
}

unsigned int VBO::genBuffer(){
	GLuint buffer;
	glGenBuffers(1, &buffer);
	return buffer;
}
