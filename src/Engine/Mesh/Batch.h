#ifndef BATCH_H
#define BATCH_H

#include "VBO.h"
#include "../Shader/Shader.h"
#include "../Entity/Camera.h"
#include <memory>

class Batch {
public:
	Batch();
	virtual ~Batch();
	
	void begin(Shader* shader, Camera* camera);
	void render(Entity* entity);
	void end();
	
	
private:
	std::vector<Entity*> entities;
	Shader* shader;
	Camera* camera;
	unsigned int lastBuffer;
};

#endif /* BATCH_H */

