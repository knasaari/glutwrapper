#include "Batch.h"

Batch::Batch() {
	lastBuffer = 0;
}

Batch::~Batch() {
}

void Batch::begin(Shader* shader, Camera* camera){
	this->entities.clear();
	this->shader = shader;
	this->camera = camera;
	lastBuffer = 0;
}

void Batch::end(){
	if(!shader->isReady())
		return;

	const Matrix4f* vmat = this->camera->getViewMatrix();
	this->shader->enable(vmat, this->camera->getProjectionMatrix(), this->camera->position);
	VertexAttributeCollection collection;
	
	for(int i=0;i<entities.size();i++){
		Entity* entity = entities[i];
		Model &model = entity->model;
		if(!model.vbo || !model.vbo->ready)
			continue;
		
		const Matrix4f* mmat = entity->getMatrix();
		this->shader->setModelMatrix(mmat);
		this->shader->setNormalMatrix(vmat, mmat);
		
		if(model.vbo->buffer != lastBuffer){
			lastBuffer = model.vbo->buffer;
			
			this->shader->unsetVertexAttributes(collection);
			
			this->shader->bindArrayBuffer(model.vbo->buffer);

			collection = model.vbo->collection;

			this->shader->setVertexAttributes(collection);
		}
		
		if(model.diffuseMap)
			this->shader->bindTexture(0, *model.diffuseMap);
				
		if(model.normalMap)
			this->shader->bindTexture(1, *model.normalMap);
		
		if(model.cubeMap)
			this->shader->bindTexture(2, *model.cubeMap);
		
		this->shader->drawArrays(model.vbo->primitiveType, 0, model.vbo->indiceCount);
		
		if(model.diffuseMap)
			this->shader->bindTexture(0, 0);
				
		if(model.normalMap)
			this->shader->bindTexture(1, 0);
	
		if(model.cubeMap)
			this->shader->bindTexture(2, 0);
	}
	
	this->shader->unsetVertexAttributes(collection);
	
	this->shader->bindArrayBuffer(0);
	this->shader->disable();
}

void Batch::render(Entity* entity){
	this->entities.push_back(entity);
}
