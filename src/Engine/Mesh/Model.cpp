#include "Model.h"

Model::Model() {
}

Model::~Model() {
}

bool Model::operator ()(const Model& a, const Model& b) const{
	return (a.vbo < b.vbo) || ((a.vbo == b.vbo) && (a.diffuseMap < b.diffuseMap)) || ((a.vbo == b.vbo) && (a.diffuseMap == b.diffuseMap) && (a.normalMap < b.normalMap));
}

bool operator ==(const Model& a, const Model& b){
	return (a.vbo == b.vbo) && (a.diffuseMap == b.diffuseMap) && (a.normalMap == b.normalMap);
}

bool operator <(const Model& a, const Model& b){
	return (a.vbo < b.vbo) || ((a.vbo == b.vbo) && (a.diffuseMap < b.diffuseMap)) || ((a.vbo == b.vbo) && (a.diffuseMap == b.diffuseMap) && (a.normalMap < b.normalMap));
}

