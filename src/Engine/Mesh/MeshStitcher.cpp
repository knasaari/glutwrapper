#include "MeshStitcher.h"
#include <iostream>

MeshStitcher::MeshStitcher() {
	vboSize = 0;
	indiceCount = 0;
	
	try{
		buffer.reserve(169173648/2);
	} catch(const std::bad_alloc &error){
		std::cout << error.what() << " reserve fail " << std::endl;
	}
}

MeshStitcher::~MeshStitcher() {
}

void MeshStitcher::add(const Mesh &mesh){
	//std::vector<float> tempBuffer;
	unsigned int tempIndices;
	std::size_t tempSize;
	
	mesh.getBufferData(buffer, tempSize, tempIndices);
	vboSize += tempSize;
	indiceCount += tempIndices;
	//buffer.insert(buffer.end(), tempBuffer.begin(), tempBuffer.end());
}

void MeshStitcher::add(Mesh mesh, const Matrix4f* transform){
	unsigned int tempIndices;
	std::size_t tempSize;
	
	if(transform != nullptr)
		mesh.transform(transform);

	//std::vector<float> tempBuffer;
	if(!mesh.getBufferData(buffer, tempSize, tempIndices)){
		std::cout << "Stitcher failed to add " << mesh.getName() << std::endl;
		return;
	}
	
	//buffer.insert(buffer.end(), tempBuffer.begin(), --tempBuffer.end());
	vboSize += tempSize;
	indiceCount += tempIndices;
	//std::cout << "size: "<< (sizeof(std::vector<float>) + (sizeof(float) * buffer.size()))/1024/1024 << "mb" << std::endl;
}

void MeshStitcher::add(Mesh mesh, const Vec3f &translate){
	unsigned int tempIndices;
	std::size_t tempSize;
	
	if(translate != Vec3f(0.0f))
		mesh.translate(translate);
		
	//std::vector<float> tempBuffer;
	if(!mesh.getBufferData(buffer, tempSize, tempIndices)){
		std::cout << "Stitcher failed to add " << mesh.getName() << std::endl;
	}
	
	//buffer.insert(buffer.end(), tempBuffer.begin(), tempBuffer.end());
	vboSize += tempSize;
	indiceCount += tempIndices;
}

void MeshStitcher::getBuffer(std::vector<float>& buffer, std::size_t& vboSize, unsigned int& indiceCount){
	buffer = this->buffer;
	vboSize = this->vboSize;
	indiceCount = this->indiceCount;
	
	std::cout << "Stitcher: i:" << indiceCount << " s:" << vboSize << " " << (float)buffer.size()*sizeof(float)/1024/1024   << "mb" << std::endl;
}

const float* MeshStitcher::getBuffer(std::size_t& vboSize, unsigned int& indiceCount){
	vboSize = this->vboSize;
	indiceCount = this->indiceCount;
	
	std::cout << "Stitcher: i:" << indiceCount << " s:" << vboSize << " " << (float)buffer.size()*sizeof(float)/1024/1024   << "mb" << std::endl;
	return buffer.data();
}
