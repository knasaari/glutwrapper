#include "InstancedBatch.h"

InstancedBatch::InstancedBatch() {
    VertexAttributeCollection collection;
    collection.add(4,5,1); // mat[0]
    collection.add(4,6,1); // mat[1]
    collection.add(4,7,1); // mat[2]
    collection.add(4,8,1); // mat[3]
    
    mbo = std::make_shared<VBO>(true);
	mbo->setVertexAttributeCollection(collection);
}

InstancedBatch::~InstancedBatch() {
}

void InstancedBatch::begin(Shader* shader, Camera* camera){
	entities.clear();
    this->shader = shader;
    this->camera = camera;
}

void InstancedBatch::draw(Model model, const std::vector<Matrix4f> &matrices){
	if(!model.vbo)
		return;
	
	std::shared_ptr<VBO> vbo = model.vbo;

	mbo->update((const float*)matrices.data(), matrices.size()*16, matrices.size());
	
	// Vertex buffer
	shader->bindArrayBuffer(vbo->buffer);
	shader->setVertexAttributes(vbo->collection);
	
	// Modelmatrix buffer
	shader->bindArrayBuffer(mbo->buffer);
	shader->setVertexAttributes(mbo->collection);
	
	if(model.diffuseMap)
		this->shader->bindTexture(0, *model.diffuseMap);
				
	if(model.normalMap)
		this->shader->bindTexture(1, *model.normalMap);
	
	if(model.cubeMap)
		this->shader->bindTexture(2, *model.cubeMap);
				
	this->shader->drawArraysInstanced(vbo->primitiveType, 0, vbo->indiceCount, matrices.size());
	
	if(model.diffuseMap)
		this->shader->bindTexture(0, 0);
				
	if(model.normalMap)
		this->shader->bindTexture(1, 0);
	
	if(model.cubeMap)
		this->shader->bindTexture(2, 0);
	
	shader->unsetVertexAttributes(mbo->collection);
	shader->unsetVertexAttributes(vbo->collection);
}

void InstancedBatch::end(){
	if(!shader->isReady())
		return;
	
	const Matrix4f* vmat = camera->getViewMatrix();
	shader->enable(vmat, camera->getProjectionMatrix(), camera->position);

	for(ModelMap::iterator it=entities.begin(); it!=entities.end(); ++it){
		draw(it->first, it->second);
	}

	shader->bindArrayBuffer(0);
	shader->disable();
}

void InstancedBatch::render(Entity* entity, bool cull){
	if(!cull || camera->frustum.sphereInFrustum(entity->position, 0.5f))
		entities[entity->model].push_back(*entity->getMatrix());
}

