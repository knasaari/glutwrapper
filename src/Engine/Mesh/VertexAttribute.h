#ifndef VERTEXATTRIBUTE_H
#define VERTEXATTRIBUTE_H

#include <vector>

class VertexAttribute {
public:
	VertexAttribute(unsigned int index, int size, int stride, int offset, unsigned int divisor = 0);
	virtual ~VertexAttribute();
	
	int getSize();
	int getStride();
	void setStride(int stride);
	
	unsigned int index, divisor;
	int size, stride, offset;
	bool normalized;
};

class VertexAttributeCollection {
public:
	VertexAttributeCollection();
	virtual ~VertexAttributeCollection();
	
	void add(int components, unsigned int index, unsigned int divisor = 0);
	
	std::vector<VertexAttribute> collection;
};

#endif /* VERTEXATTRIBUTE_H */

