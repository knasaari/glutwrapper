#include "Mesh.h"
#include <cstring>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <set>

using std::cout;

Mesh::Mesh() {
	name = "Empty mesh";
}

Mesh::~Mesh() {
}

void Mesh::loadObj(const char* filename, bool verbose){
	std::vector<std::string> file;

	try{
		file = readFile(filename);
	}catch(int e){
		throw e;
	}
	
	VectorVec3f vertices;
	std::vector<Vec2f> texcoords;
	VectorVec3f normals;
	std::vector<Face> faces;
	
	for(int i=0;i<file.size();++i){
		std::string line = file.at(i);
		
		std::size_t found = line.find_first_of(' ');
		if(found == std::string::npos){
			continue;
		}
		// to remove trailing spaces
		std::size_t lastCharacter = line.find_last_not_of(" \n\r\t");
		if(lastCharacter != std::string::npos){
			lastCharacter -= found;
		}
		
		std::string type = line.substr(0, found);
		std::string data = line.substr(found+1, lastCharacter);

		if(type.compare("v") == 0){
			try{
				vertices.push_back(this->parseToVec3f(data));
			} catch (int error){
				cout << "Error parsing v for " << filename << "\n";
			}
		}
		else if(type.compare("vt") == 0){
			try{
				texcoords.push_back(this->parseToVec2f(data));
			} catch (int error){
				cout << "Error parsing vt for " << filename << "\n";
			}
		}
		else if(type.compare("vn") == 0){
			try{
				normals.push_back(this->parseToVec3f(data));
			} catch (int error){
				cout << "Error parsing vn for " << filename << "\n";
			}
		}
		else if(type.compare("f") == 0){
			Face face;
			face.createFromString(data);
			faces.push_back(face);
		}
	}
	
	int vertexCount = vertices.size();
	int texcoordCount = texcoords.size();
	int normalCount = normals.size();
	int faceCount = faces.size();
	int totalVertexCount = 0;
	bool generateN = false;
	
	if(verbose){
		cout << "Parsing " << filename <<
			" - OBJ contains: v:" << vertexCount << "/vt:" << texcoordCount << "/vn:" << normalCount << "/f:" << faceCount << "\n";
	}
	
	try{
		for(int i=0;i<faceCount;i++) {
			Face &face = faces[i];
			totalVertexCount += (face.indices.size()-2)*3;
			face.setCorrectIndices(vertexCount, texcoordCount, normalCount);
		}
	} catch (int e){
		cout << "Error setting correct indices for " << filename << "\n";
		throw e;
	}
	
	if(normalCount == 0){
		generateN = true;
	}
	
	this->free();
	
	std::vector<Index> triangulatedIndices;
	// Break faces to triangles
	for(int f=0;f<faceCount;f++) {
		const Face &face = faces[f];
		for (int i=0; i < face.indices.size()-2; i++) {
			for(int vertex=0; vertex<3; vertex++) {
				int index = (vertex == 0)?0:i+vertex;
				triangulatedIndices.push_back(face.indices[index]);
			}
		}
	}
	
	if(generateN){
		this->computeNormals(triangulatedIndices, vertices, normals);
	}
	
	this->vertices = vertices;
	this->texcoords = texcoords;
	this->normals = normals;
	this->indices = triangulatedIndices;
	
	this->computeTangentBasis(indices,this->vertices,this->texcoords,this->normals,this->tangents,this->bitangents);

	name = std::string(filename);
	
	if(verbose){
		cout << "Loaded " << name << std::endl;
	}
}

void Mesh::computeNormals(
	std::vector<Index>& indices,
	const VectorVec3f& vertices,
	VectorVec3f& normals
	) const{
	normals.reserve(indices.size()/3);
	for (unsigned int i = 0; i < indices.size(); i += 3) {
		Index& id0 = indices[i];
		Index& id1 = indices[i + 1];
		Index& id2 = indices[i + 2];
		
		// Shortcuts for vertices
		const Vec3f& v0 = vertices[id0.v];
		const Vec3f& v1 = vertices[id1.v];
		const Vec3f& v2 = vertices[id2.v];
		
		Vec3f edge1 = v1 - v0;
		Vec3f edge2 = v2 - v0;
		
		Vec3f n = (edge1.yzx() * edge2.zxy()) - (edge1.zxy() * edge2.yzx());
		
		normals.push_back(n);
		unsigned int index = normals.size() - 1;
		id0.vn = index;
		id1.vn = index;
		id2.vn = index;
	}
}


void Mesh::computeTangentBasis (
	// inputs
	const std::vector<Index>& indices,
	const VectorVec3f& vertices,
	const std::vector<Vec2f>& uvs,
	const VectorVec3f& normals,
	// outputs
	VectorVec3f& tangents,
	VectorVec3f& bitangents
	) const{
	tangents.reserve(indices.size());
	bitangents.reserve(indices.size());
	
	for (unsigned int i = 0; i < indices.size(); i += 3) {
		const Index& id0 = indices[i];
		const Index& id1 = indices[i + 1];
		const Index& id2 = indices[i + 2];
		
		// Shortcuts for vertices
		const Vec3f& v0 = vertices[id0.v];
		const Vec3f& v1 = vertices[id1.v];
		const Vec3f& v2 = vertices[id2.v];

		// Shortcuts for UVs
		const Vec2f& uv0 = uvs[id0.vt];
		const Vec2f& uv1 = uvs[id1.vt];
		const Vec2f& uv2 = uvs[id2.vt];

		// Edges of the triangle : postion delta
		Vec3f edge1 = v1 - v0;
		Vec3f edge2 = v2 - v0;

		// UV delta
		Vec2f deltaUV1 = uv1 - uv0;
		Vec2f deltaUV2 = uv2 - uv0;

		float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
		
		Vec3f tangent = (edge1 * deltaUV2.y - edge2 * deltaUV1.y) * r;
		Vec3f bitangent = (edge2 * deltaUV1.x + edge1 * -deltaUV2.x) * r;

		// Set the same tangent for all three vertices of the triangle.
		tangents.insert(tangents.end(), 3, normalize(tangent));

		// Same thing for bitangents
		bitangents.insert(bitangents.end(), 3, normalize(bitangent));
	}
	
	std::set<unsigned int> processed;

	for (unsigned int i = 0; i < indices.size(); i++) {
		if(processed.find(i) != processed.end()){
			// index already processed
			continue;
		}
		
		std::vector<unsigned int> inds;
		Vec3f t,bt;
		// find similar indices
		for (unsigned int y = i; y < indices.size(); y++) {
			if(indices[i] == indices[y]){
				// combine similar indices t/bt
				Vec3f tan = tangents[y];
				Vec3f btan = bitangents[y];
				Vec3f n = normals[indices[y].vn];
				// Gram-Schmidt orthogonalize
				tan = normalize(tan - n * dot(n, tan));

				// Calculate handedness
				if (dot(cross(n, tan), btan) < 0.0f) {
					tan = -tan;
				}
				
				t += tan;
				bt += btan;
				
				inds.push_back(y);
			}
		}
		t = normalize(t);
		bt = normalize(bt);
		
		// assign combined values
		for (unsigned int y = 0; y < inds.size(); y++) {
			unsigned int index = inds[y];
			tangents[index] = t;
			bitangents[index] = bt;
		}
		processed.insert(inds.begin(),inds.end());
	}
}

void Mesh::free() {
	vertices.clear();
	texcoords.clear();
	normals.clear();
	indices.clear();
	name = "Empty mesh";
}

std::string Mesh::getName(){
	return name;
}

bool Mesh::getBufferData(std::vector<float>& buffer, std::size_t &vboSize, unsigned int &indiceCount) const{
	if(indices.size() == 0){
		return false;
	}
	
	bool generateTB = false;
	int VBOcomponents = 3;
	unsigned int texcoordCount = texcoords.size();
	unsigned int normalCount = normals.size();
	
	if(texcoordCount > 0){
		VBOcomponents += 2;
	}
	if(normalCount > 0){
		VBOcomponents += 3;
	}
	if(texcoordCount > 0 && normalCount > 0){
		VBOcomponents += 6;
		generateTB = true;
	}
	
	vboSize = VBOcomponents * indices.size();
	indiceCount = indices.size();
	
	unsigned int oldSize = buffer.size();
	try{
		buffer.resize(oldSize+vboSize);
	} catch(const std::bad_alloc &error){
		std::cout << error.what() << " old:" << oldSize << " new:" << oldSize+vboSize << " max:" << buffer.max_size() << " cap:" << buffer.capacity() << std::endl;
		return false;
	}
	//buffer.clear();
	//buffer.resize(vboSize+1, 0.0f);
	
//	VectorVec3f tangents;
//	VectorVec3f bitangents;
	
	float *VBO = buffer.data();
	VBO += oldSize;
//	if(generateTB){
//		this->computeTangentBasis(indices,vertices,texcoords,normals,tangents,bitangents);
//	}
	
	unsigned int c = indiceCount;
	// Generate VBO
	for(unsigned int i=0;i<c;i++) {
		const Index& in = indices[i];

		vertices[in.v].store(VBO);
		VBO += 3;
		if(texcoordCount > 0){
			memcpy(VBO, texcoords[in.vt].v, sizeof(float)*2);
			VBO += 2;
		}

		if(normalCount > 0){
			normals[in.vn].store(VBO);
			VBO += 3;
		}
		
		if(generateTB){
			tangents[i].store(VBO);
			VBO += 3;
			bitangents[i].store(VBO);
			VBO += 3;
		}
	}
	
	//cout << "Mesh " << name << " triangulated indices: " << indiceCount << ", floats: " << vboSize << std::endl;
	
	return true;
}

bool Mesh::getRawData(std::vector<float>& buffer, std::size_t &vboSize) const{
	if(indices.size() == 0){
		return false;
	}
	
	bool generateTB = false;

	unsigned int texcoordCount = texcoords.size();
	unsigned int normalCount = normals.size();
	
	if(texcoordCount > 0 && normalCount > 0){

		generateTB = true;
	}
	
	VectorVec3f tangents;
	VectorVec3f bitangents;
	
	if(generateTB){
		this->computeTangentBasis(indices,vertices,texcoords,normals,tangents,bitangents);
	}
	
	vboSize = vertices.size()*3 + texcoordCount*2 + normalCount*3 + tangents.size()*3 + bitangents.size()*3;
	
	buffer.clear();
	buffer.resize(vboSize, 0.0f);
	float *VBO = buffer.data();
	
	for(unsigned int i=0;i<vertices.size();i++) {
		vertices[i].store(VBO);
		VBO += 3;
	}
	
	for(unsigned int i=0;i<texcoordCount;i++) {
		memcpy(VBO, texcoords[i].v, sizeof(float)*2);
		VBO += 2;
	}

	for(unsigned int i=0;i<normalCount;i++) {
		normals[i].store(VBO);
		VBO += 3;
	}
	
	for(unsigned int i=0;i<tangents.size();i++) {
		tangents[i].store(VBO);
		VBO += 3;
	}
	
	for(unsigned int i=0;i<bitangents.size();i++) {
		bitangents[i].store(VBO);
		VBO += 3;
	}
	
	return true;
}

VertexAttributeCollection Mesh::getCollection() const{
	VertexAttributeCollection collection;
	
	if(vertices.size() > 0){
		collection.add(3,0); // XYZ
	}
	if(texcoords.size() > 0){
		collection.add(2,1); // UV
	}
	if(normals.size() > 0){
		collection.add(3,2); // Normal
	}
	if(texcoords.size() > 0 && normals.size() > 0){
		collection.add(3,3); // Tangent
		collection.add(3,4); // Bitangent
	}
	return collection;
}

int Mesh::transform(const Matrix4f *mat){
	const __m128 m[4] =  {_mm_set_ps(mat->m03,mat->m02,mat->m01,mat->m00),
		_mm_set_ps(mat->m13,mat->m12,mat->m11,mat->m10),
		_mm_set_ps(mat->m23,mat->m22,mat->m21,mat->m20),
		_mm_set_ps(mat->m33,mat->m32,mat->m31,mat->m30)
	};
	
	unsigned int c = vertices.size();

	for(unsigned int i=0; i<c; i++){
		Vec3f &v = vertices[i];
		mme_transformMat4(m[0],m[1],m[2],m[3], v, v);

	}
	
	c = normals.size();
	for(unsigned int i=0; i<c; i++){
		normals[i] = mme_transformMat3(m[0],m[1],m[2], normals[i]);
	}
	c = tangents.size();
	for(unsigned int i=0; i<c; i++){
		tangents[i] = mme_transformMat3(m[0],m[1],m[2], tangents[i]);
	}
	c = bitangents.size();
	for(unsigned int i=0; i<c; i++){
		bitangents[i] = mme_transformMat3(m[0],m[1],m[2], bitangents[i]);
	}
	return 1;
}

void Mesh::translate(Vec3f translation){
	unsigned int c = vertices.size();
	for(unsigned int i=0; i<c; i++){
		vertices[i] += translation;
	}
}

Vec3f Mesh::parseToVec3f(std::string& line){
	float values[3];
	
	if(sscanf(line.c_str(), "%f %f %f", values, values+1, values+2) == EOF){
		throw 1;
	}
	
	return Vec3f(values[0],values[1],values[2]);
}

Vec2f Mesh::parseToVec2f(std::string& line){
	float values[2];
	
	if(sscanf(line.c_str(), "%f %f", values, values+1) == EOF){
		throw 1;
	}
	// flip texture coordinate vertical value
	return Vec2f(values[0],(1.0f - values[1]));
}

Face::Face() {
}

Face::~Face() {
}

void Face::createFromString(const std::string& line){
	this->indices.clear();
	
	std::stringstream ss(line);
	std::string temp;
	std::vector<std::string> indexGroups;
	while(std::getline(ss, temp, ' ')){
		indexGroups.push_back(temp);
	}
	
	for(int i=0; i<indexGroups.size();i++){
		std::stringstream stream(indexGroups[i]);
		int n = 0;
		Index index;
		while(std::getline(stream, temp, '/') && n < Index::components){
			// missing index defaults to 0
			if(!temp.empty()){
				sscanf(temp.c_str(), "%i", index.a+n);
			}
			n++;
		}
		this->indices.push_back(index);
	}
}

void Face::setCorrectIndices(int vertices, int texcoords, int normals){
	for(int i=0;i<this->indices.size();i++){
		this->indices[i].v = this->getCorrectIndex(this->indices[i].v, vertices);
		this->indices[i].vt = this->getCorrectIndex(this->indices[i].vt, texcoords);
		this->indices[i].vn = this->getCorrectIndex(this->indices[i].vn, normals);
	}
}

int Face::getCorrectIndex(int index, int indexSize){
	if(index == 0)
		return 0;
	
	if(abs(index) > indexSize){
		throw 1;
	}
	
	return (index > 0)?--index:indexSize+index;
}

Index::Index() {
	v = 0;
	vt = 0;
	vn = 0;
}

Index::~Index(){
}

bool Index::operator<(const Index& that) const {
	return v < that.v || (v == that.v && vt < that.vt) || (v == that.v && vt == that.vt && vn < that.vn);
}

bool Index::operator==(const Index& that) const {
	return v == that.v && vt == that.vt && vn == that.vn;
}

Vertex::Vertex() {

}

