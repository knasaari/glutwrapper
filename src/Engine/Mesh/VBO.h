#ifndef VBO_H
#define VBO_H

#include <cstddef>
#include <vector>
#include "VertexAttribute.h"
#include "Mesh.h"

#ifdef __cplusplus
extern "C" {
#endif

class VBO{
public:
	VBO();
	VBO(bool isDynamic);
	virtual ~VBO();
	void update(const float* data, std::size_t count, unsigned int indiceCount);
	void create(const float* data, std::size_t count, unsigned int indiceCount, const VertexAttributeCollection& collection);
	void create(const Mesh& mesh);
	void setVertexAttributeCollection(const VertexAttributeCollection& collection);
	void free();
	
	unsigned int buffer;
	int indiceCount;
	std::size_t size;
	bool ready;
	unsigned int primitiveType, usage;
	VertexAttributeCollection collection;
private:
	unsigned int genBuffer();
};


#ifdef __cplusplus
}
#endif

#endif /* VBO_H */

