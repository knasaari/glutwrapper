#ifndef INSTANCEDBATCH_H
#define INSTANCEDBATCH_H

#include "VBO.h"
#include "../Shader/Shader.h"
#include "../Entity/Camera.h"
#include "Model.h"
#include <memory>
#include <map>

using ModelMap = std::map<Model, std::vector<Matrix4f>>;

class InstancedBatch {
public:
    InstancedBatch();
    virtual ~InstancedBatch();
    
    void begin(Shader* shader, Camera* camera);
    void render(Entity* entity, bool cull = true);
    void end();
private:
    ModelMap entities;
    std::shared_ptr<VBO> mbo;
    Shader* shader;
    Camera* camera;
    
    void draw(Model model, const std::vector<Matrix4f> &matrices);
};

#endif /* INSTANCEDBATCH_H */

