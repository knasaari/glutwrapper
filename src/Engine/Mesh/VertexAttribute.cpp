#include "VertexAttribute.h"

VertexAttribute::VertexAttribute(unsigned int index, int size, int stride, int offset, unsigned int divisor) {
	this->index = index;
	this->size = size;
	normalized = false;
	this->stride = stride;
	this->offset = offset;
	this->divisor = divisor;
}

VertexAttribute::~VertexAttribute() {
}

int VertexAttribute::getSize(){
	return this->size;
}

int VertexAttribute::getStride(){
	return this->stride;
}

void VertexAttribute::setStride(int stride){
	this->stride = stride;
}

VertexAttributeCollection::VertexAttributeCollection() {

}

VertexAttributeCollection::~VertexAttributeCollection(){
}

void VertexAttributeCollection::add(int components, unsigned int index, unsigned int divisor){
	int otherComponents = 0;
	for(int i=0;i<collection.size();i++){
		collection[i].setStride(collection[i].getStride() + components * sizeof(float));
		otherComponents += collection[i].getSize();
	}
	
	VertexAttribute attribute(index, components, (components+otherComponents)*sizeof(float), otherComponents*sizeof(float), divisor);
	
	collection.push_back(attribute);
}
