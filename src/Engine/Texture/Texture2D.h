#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include "Texture.h"


class Texture2D : public Texture{
public:
	Texture2D();
	virtual ~Texture2D();
	unsigned int loadPNG(const std::string& filename);
	unsigned int makeTexture(unsigned char* data, unsigned int width, unsigned int height, int colorComponents);
private:

};

#endif /* TEXTURE2D_H */

