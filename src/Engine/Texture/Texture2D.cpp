#include "Texture2D.h"
#include "glad/glad.h"
#include <iostream>
#include "picopng.h"
#include "../../utility.h"

Texture2D::Texture2D() : Texture(){
	target = GL_TEXTURE_2D;
}

Texture2D::~Texture2D() {
}

unsigned int Texture2D::loadPNG(const std::string& filename){
	std::vector<unsigned char> pngData, decodedPNG;
	unsigned long w, h;
	
	loadFile(pngData, filename);
	decodePNG(decodedPNG, w, h, pngData.data(), pngData.size());
	return makeTexture((GLubyte*)decodedPNG.data(), w, h, 4);
}

unsigned int Texture2D::makeTexture(unsigned char* data, unsigned int width, unsigned int height, int colorComponents) {
	std::cout << "Creating texture with w: " << width << ", h: " << height << "\n";
	GLenum format = GL_RGB;
	if(colorComponents == 4)
		format = GL_RGBA;
	
	GLuint handle = genTexture();

	glBindTexture(target, handle);

	// Create a texture 
	glTexImage2D(target, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, (GLvoid*)data);

	// Set up the texture
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	float af = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &af);
	glTexParameterf(target, GL_TEXTURE_MAX_ANISOTROPY_EXT, af); 

	glBindTexture(target, 0);

	return handle;
}
