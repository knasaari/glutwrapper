#ifndef TEXTURECUBE_H
#define TEXTURECUBE_H

#include "Texture.h"
#include <vector>

class TextureCube : public Texture {
private:
	struct Size{
		unsigned long w,h;
	};
	unsigned int makeTexture(std::vector<unsigned char>* data, TextureCube::Size *size, int colorComponents);
public:
	TextureCube();
	virtual ~TextureCube();
	unsigned int loadPNG(
		const std::string& right,
		const std::string& left,
		const std::string& up,
		const std::string& down,
		const std::string& front,
		const std::string& back
	);
	

};

#endif /* TEXTURECUBE_H */

