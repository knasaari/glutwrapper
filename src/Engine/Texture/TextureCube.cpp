#include "TextureCube.h"
#include "glad/glad.h"
#include <iostream>
#include "picopng.h"
#include "../../utility.h"

TextureCube::TextureCube() : Texture(){
	target = GL_TEXTURE_CUBE_MAP;
}

TextureCube::~TextureCube() {
}

unsigned int TextureCube::loadPNG(
		const std::string& right,
		const std::string& left,
		const std::string& up,
		const std::string& down,
		const std::string& front,
		const std::string& back){
	std::vector<std::string> filelist(6);
	filelist[0] = right;
	filelist[1] = left;
	filelist[2] = up;
	filelist[3] = down;
	filelist[4] = front;
	filelist[5] = back;
	
	std::vector<unsigned char> pngData;
	std::vector<unsigned char> decodedPNG[6];
	Size size[6];
	
	for(unsigned int i=0;i<6;i++){
		loadFile(pngData, filelist[i]);
		decodePNG(decodedPNG[i], size[i].w, size[i].h, pngData.data(), pngData.size());
		
		if(size[i].w != size[0].w || size[i].h != size[0].h){
			std::cout << "Cube map sides must have identical dimensions. Invalid side: " << filelist[i] << std::endl;
			return 0;
		}
	}
	
	return makeTexture(decodedPNG, size, 4);
}

unsigned int TextureCube::makeTexture(std::vector<unsigned char>* data, TextureCube::Size *size, int colorComponents){
	std::cout << "Creating cube texture\n";
	GLenum format = GL_RGB;
	if(colorComponents == 4)
		format = GL_RGBA;
	
	GLuint handle = genTexture();
	glBindTexture(target, handle);
	
	// Create a texture 
	for(int i=0;i<6;i++){
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, size[i].w, size[i].h, 0, format, GL_UNSIGNED_BYTE, (GLvoid*)data[i].data());
	}
	
	// Set up the texture
	glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	float af = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &af);
	glTexParameterf(target, GL_TEXTURE_MAX_ANISOTROPY_EXT, af); 

	glBindTexture(target, 0);
	return handle;
}

