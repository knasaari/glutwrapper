#include "Texture.h"
#include <glad/glad.h>
#include <iostream>

Texture::Texture() {
	handle = 0;
	target = 0;
}

Texture::~Texture() {
	free();
}

void Texture::free(){
	if(handle != 0){
		std::cout << "Freeing texture " << handle << std::endl;
		glDeleteTextures(1, &handle);
	}
	
	handle = 0;
}

unsigned int Texture::getHandle() const{
	return handle;
}

unsigned int Texture::getTarget() const{
	return target;
}

void Texture::setFiltering(bool linear){
    if(handle !=0){
		unsigned int filter = linear?GL_LINEAR:GL_NEAREST;
		glBindTexture(target, handle);
		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
		glBindTexture(target, 0);
    }
}

unsigned int Texture::genTexture(){
	free();
	glGenTextures(1, &handle);
	return handle;
}

