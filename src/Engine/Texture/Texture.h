#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>

class Texture {
public:
	Texture();
	virtual ~Texture();
	void free();
	unsigned int getHandle() const;
	unsigned int getTarget() const;
        void setFiltering(bool linear);
private:
	unsigned int handle;
protected:
	unsigned int genTexture();
	unsigned int target;
};

#endif /* TEXTURE_H */

