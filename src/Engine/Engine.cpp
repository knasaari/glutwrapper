#include "Engine.h"
#include <cmath>
#include <sstream>
#include "Shader/Shader.h"
#include "picopng.h"
#include "Mesh/Batch.h"
#include "Mesh/MeshStitcher.h"

using SharedTexture = std::shared_ptr<Texture>;
using SharedTexture2D = std::shared_ptr<Texture2D>;
using SharedTextureCube = std::shared_ptr<TextureCube>;

Engine::Engine()
	: camera(0.01f, 100.0f, 70.0f, (float)Engine::WINDOW_WIDTH/(float)Engine::WINDOW_HEIGHT),
	uiCamera(0.01f, 100.0f, 0, (float)Engine::WINDOW_WIDTH, (float)Engine::WINDOW_HEIGHT, 0){
	phase = 0;
	tick = 0;
	shaderSelect = 0;
	entityCount = 9;
	renderToggle = true;
}

void Engine::load(){
	Vec4f *ptt = new Vec4f(1.0f);
	
	camera.position.setY(sum(*ptt));
	camera.position.setZ(5.0f);
	camera.turnLocal(Vec3f(0,0.5f,0));
	camera.turn(Vec3f(0.5f,0,0));
	
	shader = new Shader("./shader/normalmap.vert","./shader/normalmap.frag");
	shader->create();

	shaderUi = new Shader("./shader/flat.vert","./shader/flat.frag");
	shaderUi->create();
	
	shaderSimple = new Shader("./shader/simple_instanced.vert","./shader/simple_directional.frag");
	shaderSimple->create();
	
	shaderFlat = new Shader("./shader/simple_instanced.vert","./shader/flat.frag");
	shaderFlat->create();
	
	shaderSkybox = new Shader("./shader/skybox.vert","./shader/skybox.frag");
	shaderSkybox->create();
	
	shaderInstanced = new Shader("./shader/instanced.vert","./shader/normalmap.frag");
	shaderInstanced->create();
	
	Entity::initBinMeshes();
	std::shared_ptr<VBO> vbo[8];
	
	Mesh mesh;
	mesh.loadObj("../assets/bulb.obj");
	vbo[0] = std::make_shared<VBO>();
	vbo[0]->create(mesh);
	mesh.loadObj("../assets/asteroid2_min_mapped.obj");
	vbo[1] = std::make_shared<VBO>();
	vbo[1]->create(mesh);
	mesh.loadObj("../assets/skybox.obj");
	vbo[2] = std::make_shared<VBO>();
	vbo[2]->create(mesh);
	mesh.loadObj("../assets/asteroid_scaled.obj");
	vbo[3] = std::make_shared<VBO>();
	vbo[3]->create(mesh);
	
	Mesh track_x, track_t, track_straight, track_90;
	track_x.loadObj("../assets/track/track_x_cross.obj");
	vbo[4] = std::make_shared<VBO>();
	vbo[4]->create(track_x);
	track_t.loadObj("../assets/track/track_t_cross.obj");
	vbo[5] = std::make_shared<VBO>();
	vbo[5]->create(track_t);
	track_straight.loadObj("../assets/track/track.obj");
	vbo[6] = std::make_shared<VBO>();
	vbo[6]->create(track_straight);
	track_90.loadObj("../assets/track/track_90_corner.obj");
	vbo[7] = std::make_shared<VBO>();
	vbo[7]->create(track_90);
	
	SharedTexture2D dMap = std::make_shared<Texture2D>();
	SharedTexture2D nMap = std::make_shared<Texture2D>();
	SharedTexture2D bulbDMap = std::make_shared<Texture2D>();
	SharedTexture2D bulbNMap = std::make_shared<Texture2D>();
	SharedTexture2D asScaledDiffuse = std::make_shared<Texture2D>();
	SharedTexture2D asScaledNormal = std::make_shared<Texture2D>();
	SharedTexture2D trackDiffuse = std::make_shared<Texture2D>();
	SharedTexture2D trackNormal = std::make_shared<Texture2D>();
	SharedTexture2D uiDiffuse = std::make_shared<Texture2D>();
	SharedTextureCube skyboxDiffuse = std::make_shared<TextureCube>();
	
	bulbDMap->loadPNG("../assets/bulb_d.png");
	bulbNMap->loadPNG("../assets/bulb_n.png");
//	dMap->loadPNG("../assets/as2_d.png");
//	nMap->loadPNG("../assets/as2_n.png");
//	asScaledDiffuse->loadPNG("../assets/asteroid_d.png");
//	asScaledNormal->loadPNG("../assets/asteroid_n_super.png");
	trackDiffuse->loadPNG("../assets/track/track_texture.png");
	trackNormal->loadPNG("../assets/track/track_n.png");
	uiDiffuse->loadPNG("../assets/ui.png");
	uiDiffuse->setFiltering(false);
	skyboxDiffuse->loadPNG(
		"../assets/skybox/hazy0.png",
		"../assets/skybox/hazy1.png",
		"../assets/skybox/hazy2.png",
		"../assets/skybox/hazy3.png",
		"../assets/skybox/hazy4.png",
		"../assets/skybox/hazy5.png"
	);
	
	Model asteroidModel, asteroidScaledModel, lightbulbModel, uiModel, skyboxModel;
	Model track[4];
	
	asteroidModel.vbo = vbo[1];
	asteroidModel.diffuseMap = dMap;
	asteroidModel.normalMap = nMap;
	
	asteroidScaledModel.vbo = vbo[3];
	asteroidScaledModel.diffuseMap = asScaledDiffuse;
	asteroidScaledModel.normalMap = asScaledNormal;

	map.generate();
	map.model.diffuseMap = trackDiffuse;
	map.model.normalMap = trackNormal;
	
	track[0].vbo = vbo[4]; // X
	track[0].diffuseMap = trackDiffuse;
	track[0].normalMap = trackNormal;
	track[1] = track[0]; // T
	track[1].vbo = vbo[5];
	track[2] = track[0];  // straight
	track[2].vbo = vbo[6];
	track[3] = track[0]; // bend
	track[3].vbo = vbo[7];
	
	unsigned int i=0;
	/*
	for(int y=1;y<20;y++){
	    box[i].setPosition(Vec3f(0,0,y));
	    box[i].model = track[2];
//		stitcher.add(track_straight, box[i].getMatrix());
	    i++;
	    box[i].setPosition(Vec3f(20,0,y));
	    box[i].model = track[2];
//		stitcher.add(track_straight, box[i].getMatrix());
	    i++;
	    box[i].setPosition(Vec3f(y,0,0));
	    box[i].setRotation(Vec3f(0,3.141f/2,0));
	    box[i].model = track[2];
//		stitcher.add(track_straight, box[i].getMatrix());
	    i++;
	    box[i].setPosition(Vec3f(y,0,20));
	    box[i].setRotation(Vec3f(0,3.141f/2,0));
	    box[i].model = track[2];
//		stitcher.add(track_straight, box[i].getMatrix());
	    i++;
	}
	box[i].setPosition(Vec3f(0,0,0));
//	stitcher.add(track_90, box[i].getMatrix());
	box[i++].model = track[3];
	
	box[i].setPosition(Vec3f(20,0,0));
	box[i].setRotation(Vec3f(0,-3.141f/2,0));
//	stitcher.add(track_90, box[i].getMatrix());
	box[i++].model = track[3];
	
	box[i].setPosition(Vec3f(0,0,20));
	box[i].setRotation(Vec3f(0,-3.141f*1.5,0));
//	stitcher.add(track_90, box[i].getMatrix());
	box[i++].model = track[3];
	
	box[i].setPosition(Vec3f(20,0,20));
	box[i].setRotation(Vec3f(0,-3.141f,0));
//	stitcher.add(track_90, box[i].getMatrix());
	box[i++].model = track[3];
	
	box[0].makeOrigo();
	*/
	
	
	for(i;i<239;i++){
		box[i].setPosition(Vec3f(i%20,i/400,(i/20)%20));
//		box[i].setPosition(Vec3f(i%128,0,(i/128)%128));
		//box[i].setPosition(Vec3f(i%63,0,(i/64)));
		
		//box[y].model = asteroidModel;
		box[i].model = track[rand()%4];
		
		//if(y%2 == 0)
		//	box[y].model = asteroidModel;
		//else if(y%2 == 1)
		//	box[y].model = asteroidScaledModel;
		
		//else
		//	box[y].model = rockModel;	
	}
	
	entityCount = i;
	
	lightbulbModel.vbo = vbo[0];
	lightbulbModel.diffuseMap = bulbDMap;
	lightbulbModel.normalMap = bulbNMap;
	lightbulb.model = lightbulbModel;
	lightbulb.setScale(Vec3f(0.5));
	
	skyboxModel.vbo = vbo[2];
	skyboxModel.cubeMap = skyboxDiffuse;
	skybox.model = skyboxModel;
	
	std::vector<float> uiBuffer;
	this->makePlane(uiBuffer, Vec3f(), Vec3f(256,128,0), Vec2f(), Vec2f(1,1));
	
	VertexAttributeCollection collection;
	collection.add(3,0); // xyz
	collection.add(2,1); // uv
	
	std::shared_ptr<VBO> uiPlane = std::make_shared<VBO>();
	uiPlane->create(uiBuffer.data(), uiBuffer.size(), 6, collection);
	
	uiModel.vbo = uiPlane;
	uiModel.diffuseMap = uiDiffuse;
	ui.model = uiModel;		
	ui.setPosition(Vec3f(0,5,0));
	uiCamera.setPosition(Vec3f(0,0,10));
	
	unsigned int vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
}

void Engine::update() {
	if(mouseDown[0] == true){
		float x = mouseDelta().y * 0.00174532925f;
		float y = mouseDelta().x * 0.00174532925f;
		camera.turn(Vec3f(x,0,0));
		camera.turnLocal(Vec3f(0,y,0));
	}
	
	if(mouseDown[2] == true){
		float x = mouseDelta().y * 0.00174532925f;
		float y = mouseDelta().x * 0.00174532925f;
		if(keyDown('z'))
			for(int i=0;i<entityCount;i++){
				box[i].turn(Vec3f(-x,-y,0));
			}
		else
			for(int i=0;i<entityCount;i++){
				box[i].move(Vec3f(-x,0,y));
			}
	}

	if(keyDown('W') || keyDown('S')){
		float mul = (keyDown('W'))?-1:1;
		camera.moveLocal(Vec3f(0,0,mul*0.1f*appSpeed()));
	}
	
	if(keyDown('A') || keyDown('D')){
		float mul = (keyDown('A'))?-1:1;
		camera.moveLocal(Vec3f(mul*0.1f*appSpeed(),0,0));
	}
	
	if(keyDown('1')){
		std::chrono::high_resolution_clock::time_point preLoad = std::chrono::high_resolution_clock::now();
		for(int n=0;n<10000;n++)
		for(int i=0;i<entityCount;i++){
			box[i].turnLocal(Vec3f(0.05*appSpeed(),0,0));
		}
		
		std::chrono::high_resolution_clock::time_point afterLoad = std::chrono::high_resolution_clock::now();
		std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(afterLoad - preLoad);
		std::cout << "turn took: " << ms.count() << "ms" << std::endl;
	}
	if(keyDown('2')){
		for(int i=0;i<entityCount;i++){
			box[i].turnLocal(Vec3f(0,0.05*appSpeed(),0));
		}
	}
	if(keyDown('3')){
		for(int i=0;i<entityCount;i++){
			box[i].turnLocal(Vec3f(0,0.0,0.05*appSpeed()));
		}
	}
	if(keyDown('4')){
		for(int i=0;i<entityCount;i++){
			box[i].setRotation(Vec3f(0,0,0));
		}
	}

	if(keyDown('5')){
		for(int i=0;i<entityCount;i++){
			box[i].moveLocal(Vec3f(0,0,0.05*appSpeed()));
		}
	}
	if(keyDown('6')){
		for(int i=0;i<entityCount;i++){
			box[i].move(Vec3f(0,0,0.05*appSpeed()));
		}
	}
	
	if(keyHit('R') == true) {
		shader->refresh();
		shaderFlat->refresh();
		shaderSimple->refresh();
		shaderSkybox->refresh();
		shaderInstanced->refresh();
	}
	
	if(keyHit('N') == true) {
		shaderSelect++;
	}
	
	if(keyHit('M') == true) {
		renderToggle = !renderToggle;
	}
	
	if(keyHit('G') == true) {
	    map.generate();
	}
	
	if(keyDown('I') == true) {
		entityCount += (entityCount < 10000)?1:0;
	}
	if(keyDown('U') == true) {
		entityCount -= (entityCount > 1)?1:0;
	}
	
	if(keyHit('B') == true) {
		for (int i = 0; i < entityCount; i++) {
			float x = 0.5f+0.25f*(rand()%10);
			float y = 0.5f+0.25f*(rand()%10);
			float z = 0.5f+0.25f*(rand()%10);
			box[i].setScale(Vec3f(x,y,z));
		}
	}
	
	if(keyHit('Q') == true){
		stop();
	}
	
	this->lightPos = Vec4f(sinf(phase*0.05f)*14,1,cosf(phase*0.05f)*14,1);
	this->shader->setLightPos(lightPos);
	this->shaderSimple->setLightPos(lightPos);
	this->shaderInstanced->setLightPos(lightPos);
	lightbulb.setPosition(Vec3f(lightPos.x(), lightPos.y(), lightPos.z()));
	
	//phase += 0.3f*appSpeed();
	tick += appSpeed();
	/*
	if(tick > 60){
		tick = tick-60.0f;
		this->updateTextureCheckers(textureData, texture, textureWidth, textureHeight, colorComponents);
	}*/
}

void Engine::display() {
	glDepthMask(GL_FALSE);
	Batch batch3;
	batch3.begin(shaderSkybox, &camera);
	batch3.render(&skybox);
	batch3.end();
	glDepthMask(GL_TRUE);
	
	Shader* selected;
	switch(shaderSelect%3){
		case 0:
			selected = shaderInstanced;
			break;
		case 1:
			selected = shaderSimple;
			break;
		case 2:
			selected = shaderFlat;
			break;
	}
	
	batch.begin(selected, &camera);
	if(!renderToggle)
	    for(int i=0;i<entityCount;i++){
	    	batch.render(box+i);
	    }
	else{
	    batch.render(&map, false);
	}
	batch.render(&lightbulb);
	batch.end();

	Batch uiBatch;
	uiBatch.begin(shaderUi, &uiCamera);
	uiBatch.render(&ui);
	uiBatch.end();
	
}

void Engine::reshape(int width, int height) {
	this->camera.set(0.01f, 100.0f, 70.0f, (float)width/(float)height);
	this->uiCamera.set(0.01f, 100.0f, 0, (float)width, (float)height, 0);
}

void Engine::makePlane(std::vector<float>& buffer, const Vec3f& p0, const Vec3f& p1, const Vec2f& uv0, const Vec2f& uv1){
    buffer.clear();
    buffer.resize(5*6, 0.0f);
    int j = 0;
    
    buffer[j++] = p0.x();
    buffer[j++] = p0.y();
    buffer[j++] = 0;
    buffer[j++] = uv0.x;
    buffer[j++] = uv0.y;

    buffer[j++] = p0.x();
    buffer[j++] = p1.y();
    buffer[j++] = 0;
    buffer[j++] = uv0.x;
    buffer[j++] = uv1.y;

    buffer[j++] = p1.x();
    buffer[j++] = p1.y();
    buffer[j++] = 0;
    buffer[j++] = uv1.x;
    buffer[j++] = uv1.y;

    buffer[j++] = p0.x();
    buffer[j++] = p0.y();
    buffer[j++] = 0;
    buffer[j++] = uv0.x;
    buffer[j++] = uv0.y;

    buffer[j++] = p1.x();
    buffer[j++] = p1.y();
    buffer[j++] = 0;
    buffer[j++] = uv1.x;
    buffer[j++] = uv1.y;

    buffer[j++] = p1.x();
    buffer[j++] = p0.y();
    buffer[j++] = 0;
    buffer[j++] = uv1.x;
    buffer[j++] = uv0.y;
}
