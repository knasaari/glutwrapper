#ifndef UTILITY_H
#define	UTILITY_H

#include "matrix4.h"
#include <vector>
#include <cstring>
#include <string>
#include <xmmintrin.h>
#include "vec.h"

#define PI 3.14159265358979323846

float degToRad(float degrees);
float radToDeg(float radians);

Vec4f eulerToQuaternion(const Vec3f& euler);
void quaternionToMat4(Matrix4f* matrix, const Vec4f& q);
Vec4f mulQuaternion(Vec4f q1, Vec4f q2);
inline Vec3f rotateVec3f(Vec4f quaternion, Vec3f v){
	Vec3f q3(quaternion.m);
	float s = quaternion.w();
	
	return 2.0f * dot(q3, v) * q3
		+ (s*s - dot(q3,q3)) * v
		+ 2.0f * s * cross(q3,v);
}
Vec4f axisAngleToQuaternion(const Vec4f& axisAngle);
void mme_transformMat4Vec3(const Matrix4f *left, const float *right, float w, float *dest);
inline void mme_transformMat4(__m128 m0, __m128 m1, __m128 m2, __m128 m3, Vec3f right, Vec3f &dest){
#ifdef SIMD
	const __m128 xxxx = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(0, 0, 0, 0));
	const __m128 yyyy = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(1, 1, 1, 1));
	const __m128 zzzz = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(2, 2, 2, 2));
#else
	const __m128 xxxx = _mm_set1_ps(right.x());
	const __m128 yyyy = _mm_set1_ps(right.y());
	const __m128 zzzz = _mm_set1_ps(right.z());
#endif
	const __m128 wwww = _mm_set1_ps(1.0f); // TODO: fix this

	__m128 result = _mm_mul_ps(m0, xxxx);
	result = _mm_add_ps(result, _mm_mul_ps(m1, yyyy));
	result = _mm_add_ps(result, _mm_mul_ps(m2, zzzz));
	result = _mm_add_ps(result, _mm_mul_ps(m3, wwww));

#ifdef SIMD
	dest.m = result;
#else
	_mm_storeu_ps(dest.v, result);
#endif
}
inline Vec3f mme_transformMat3(__m128 m0, __m128 m1, __m128 m2, Vec3f right){
#ifdef SIMD
	__m128 xxxx = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(0, 0, 0, 0));
	__m128 yyyy = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(1, 1, 1, 1));
	__m128 zzzz = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(2, 2, 2, 2));
#else
	const __m128 xxxx = _mm_set1_ps(right.x());
	const __m128 yyyy = _mm_set1_ps(right.y());
	const __m128 zzzz = _mm_set1_ps(right.z());
#endif

	__m128 result =_mm_add_ps(
		_mm_add_ps(
			_mm_mul_ps(m0, xxxx),
			_mm_mul_ps(m1, yyyy)
		),
		_mm_mul_ps(m2, zzzz)
	);
#ifdef SIMD
	return Vec3f(result);
#else
	Vec3f res;
	_mm_storeu_ps(res.v, result);
	return res;
#endif	
}

void saveBinaryData(const char* filename, const void* data, unsigned int size, unsigned int count);
void saveCharData(const char* filename, const void* data, unsigned int size, unsigned int count);
std::string readFileToString(const char* filename);
std::vector<std::string> readFile(const char* filename);
void loadFile(std::vector<unsigned char>& buffer, const std::string& filename);
std::ostream& operator<< (std::ostream& stream, Vec4f v);

#endif	/* UTILITY_H */
