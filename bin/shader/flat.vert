#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;

out vec2 TexCoords;

void main (void){
	TexCoords = OBJ_Texcoord;

	gl_Position = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1);
}