#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;
in float OBJ_TexUnit;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;

out float Texunit;
out vec2 Texcoord;
out vec3 Normal;
out vec3 eyePos, eyeNormal;
out vec3 lightDir;

void main (void){
	vec4 pos = vec4(OBJ_Position.xyz, 1.0);
	Texcoord = OBJ_Texcoord;
	Texunit = 0;
	
	lightDir = mat3(ViewMatrix) * normalize(vec3(1,1,1));
	eyePos = vec3(ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1));
	eyeNormal = normalize(vec3(ViewMatrix * ModelMatrix * vec4(OBJ_Normal, 0)));
	
	gl_Position = ProjectionMatrix * vec4(eyePos, 1);
	eyePos = -eyePos;
}