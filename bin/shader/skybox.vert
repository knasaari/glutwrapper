#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;
in float OBJ_TexUnit;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;

out vec3 Texcoord;

void main (void){
	Texcoord = OBJ_Position;
	mat4 View = mat4(mat3(ViewMatrix));
	gl_Position = ProjectionMatrix * View * vec4(OBJ_Position, 1);
}