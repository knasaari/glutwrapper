#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform samplerCube Difftexture2;
uniform sampler2D Difftexture3;

in vec3 Texcoord;
out vec4 FragColor;

void main (void){
	vec4 diffuse = texture(Difftexture2, Texcoord);

	FragColor = diffuse;
}