#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 NormalMatrix;
uniform vec4 lightPos;

out vec2 TexCoords;
out vec3 fragPos, eyeNormal;
out vec3 lightPosView;

void main (void){
	vec4 pos = vec4(OBJ_Position.xyz, 1.0);
	TexCoords = OBJ_Texcoord;
	
	lightPosView = vec3(ViewMatrix * lightPos);
	fragPos = vec3(ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1));
	eyeNormal = normalize(mat3(NormalMatrix) * OBJ_Normal);
	
	gl_Position = ProjectionMatrix * vec4(fragPos, 1);

}