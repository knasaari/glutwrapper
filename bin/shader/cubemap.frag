#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform samplerCube Difftexture2;
uniform sampler2D Difftexture3;

uniform int Normalmapping;

in vec3 FragPos;
in vec2 TexCoords;
in vec3 TangentLightPos;
in vec3 TangentViewPos;
in vec3 TangentFragPos;
in mat3 TBN;
out vec4 FragColor;

void main (void){
	// Obtain normal from normal map in range [0,1]
	vec4 normalMap = texture(Difftexture1, TexCoords);
	vec3 normal = normalMap.rgb;
	
	
	// Transform normal vector to range [-1,1]
	normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space
 	normal = normalize(TBN * normal);

	float ratio = 1.00 / 1.52;
	vec3 I = normalize(TangentFragPos - TangentViewPos);
	vec3 R = reflect(I, normal);
	//vec3 R = refract(I, normal, ratio);
	vec3 reflection = texture(Difftexture2, R).rgb;
	FragColor = vec4(reflection, 1);
}
