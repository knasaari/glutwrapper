#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform samplerCube Difftexture2;
uniform sampler2D Difftexture3;

in vec2 TexCoords;
out vec4 FragColor;

void main (void){
	vec4 diffuse = texture(Difftexture0, TexCoords);
	if(diffuse.a < 0.5)
		discard;

	FragColor = diffuse;
}