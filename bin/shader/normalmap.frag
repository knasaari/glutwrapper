#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform samplerCube Difftexture2;
uniform sampler2D Difftexture3;

uniform int Normalmapping;

in vec2 TexCoords;
in vec3 LightPos;
in vec3 ViewPos;
in vec3 FragPos;
in mat3 TBN;
out vec4 FragColor;

void main (void){
	// Obtain normal from normal map in range [0,1]
	vec4 normalMap = texture(Difftexture1, TexCoords);
	vec3 normal = normalMap.rgb;
	
	// Transform normal vector to range [-1,1]
	normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space
 //	normal = normalize(TBN * normal);
//	float intensity = max(dot(eyeNormal, lightDir), 0.0);
	
	// Get diffuse color
	vec4 colorTexture = texture(Difftexture0, TexCoords);
	vec3 color = vec3(colorTexture);
	float specLevel = colorTexture.a;
	//color = vec3(0.5);

	// Diffuse
	//vec3 lightDir = normalize(LightPos - FragPos);
	vec3 lightDir = normalize(LightPos); // directional light
	
	float diff = max(dot(lightDir,normal), 0.0);
	vec3 diffuse = diff * color;
	
	// Ambient
	vec3 ambient = 0.15 * color;
	
	// Specular
	vec3 viewDir = normalize(ViewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir + viewDir);  
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 10.0);
	vec3 specular = color * spec;
	
	//float distance = length(LightPos - FragPos);
	//float constant = 1.0;
	//float linear = 0.09;
	//float quadratic = 0.032;	 
	//float attenuation = 1.0 / (constant + linear * distance + quadratic * (distance * distance));
	//diffuse *= attenuation;
	//ambient *= attenuation;
	//specular *= attenuation;
	
	FragColor = vec4(ambient + diffuse + specular, 1.0f);
	//gl_FragColor = vec4(normal, 1.0f);
}
