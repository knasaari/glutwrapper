#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;
in vec3 OBJ_Tangent;
in vec3 OBJ_Bitangent;
in mat4 InstanceMatrix;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
uniform vec3 viewPos;
uniform vec4 lightPos;

out vec2 TexCoords;
out vec3 LightPos;
out vec3 ViewPos;
out vec3 FragPos;
out mat3 TBN;

void main (void){
	FragPos = vec3(InstanceMatrix * vec4(OBJ_Position, 1.0));   
	gl_Position =  ProjectionMatrix * ViewMatrix * vec4(FragPos, 1);
	
	TexCoords = OBJ_Texcoord;
    
	mat3 normalMatrix = transpose(inverse(mat3(InstanceMatrix)));
	vec3 T = normalize(normalMatrix * OBJ_Tangent);
	vec3 N = normalize(normalMatrix * OBJ_Normal);  
	T = normalize(T - dot(T, N) * N);
	//vec3 B = normalize(normalMatrix * OBJ_Bitangent);
	vec3 B = cross(N, T);
	TBN = mat3(T, B, N);
	mat3 TBNt = transpose(TBN);
	FragPos = TBNt * FragPos;
	LightPos = TBNt * vec3(1,1,0.3);
	//LightPos = TBNt * lightPos.xyz;
	ViewPos  = TBNt * viewPos;
}
