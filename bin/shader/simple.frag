#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform sampler2D Difftexture2;
uniform sampler2D Difftexture3;

in vec2 Texcoord;
in float Texunit;
in vec3 eyePos, eyeNormal, lightDir;
out vec4 FragColor;
void main (void){
	float index = max(0, min(3, floor(Texunit + 0.5)) );
	vec4 diffuse;
	vec4 specular = vec4(0.0);
	vec4 ambient = vec4(0.1,0.1,0.1,1.0);
	vec3 eye = normalize(eyePos);
		
	float shininess = 55.0;
	vec4 specularColor = vec4(0.5, 0.5, 0.5, 1);
	
	float intensity = max(dot(eyeNormal, lightDir), 0.0);
	
	if(index < 1.0){
		diffuse = texture(Difftexture0, Texcoord);
	}
	else if(index < 2.0){
		diffuse = texture(Difftexture1, Texcoord);
	}
	else if(index < 3.0){
		diffuse = texture(Difftexture2, Texcoord);
	}
	else if(index < 4.0){
		diffuse = texture(Difftexture3, Texcoord);
	}
	else{
		diffuse = vec4(0.5,0.5,0.5,1.0);
	}
	
	//if(diffuse.a < 0.4){
	//	discard;
	//}
	
	if(intensity > 0.0){
		vec3 h = normalize(lightDir + eye);
		float intSpec = max(dot(h,eyeNormal), 0.0);
		specular = vec4(vec3(diffuse),1) * pow(intSpec, shininess);
	}
	
	FragColor = ambient * diffuse + intensity * diffuse + specular;
	//gl_FragColor = diffuse;
}