#version 150

in vec2 OBJ_Texcoord;
in vec3 OBJ_Position;
in vec3 OBJ_Normal;
in vec3 OBJ_Tangent;
in vec3 OBJ_Bitangent;

uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 NormalMatrix;
uniform mat4 ModelNormalMatrix;
uniform vec3 viewPos;
uniform vec4 lightPos;

out vec3 FragPos;
out vec2 TexCoords;
out vec3 TangentLightPos;
out vec3 TangentViewPos;
out vec3 TangentFragPos;
out mat3 TBN;

void main (void){
	gl_Position =  ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(OBJ_Position, 1);

	FragPos = vec3(ModelMatrix * vec4(OBJ_Position, 1.0));   
	TexCoords = OBJ_Texcoord;
    
	mat3 normalMatrix = mat3(ModelNormalMatrix);
	vec3 T = normalize(normalMatrix * OBJ_Tangent);
	vec3 N = normalize(normalMatrix * OBJ_Normal);  
	T = normalize(T - dot(T, N) * N);
	//vec3 B = normalize(normalMatrix * OBJ_Bitangent);
	vec3 B = cross(N, T);
	TBN = mat3(T, B, N);
	TangentLightPos = vec3(lightPos);
	TangentViewPos  = viewPos;
	TangentFragPos  = FragPos;
}