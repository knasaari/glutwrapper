#version 150

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
uniform samplerCube Difftexture2;
uniform sampler2D Difftexture3;

in vec2 TexCoords;
in vec3 LightPos;
in vec3 ViewPos;
in vec3 FragPos;
in vec3 Normal;
out vec4 FragColor;

void main (void){
	vec4 diffuse;
	vec4 specular = vec4(0.0);

	float shininess = 10.0;

	vec3 lightDir = normalize(LightPos - FragPos);
	//vec3 lightDir = normalize(LightPos);

	float diff = max(dot(lightDir, Normal), 0.0);
	
	diffuse = texture(Difftexture0, TexCoords);
	//diffuse = vec4(0.5,0.5,0.5,1);
	vec4 ambient = 0.05 * diffuse;
	
	if(diff > 0.0){
		vec3 viewDir = normalize(ViewPos - FragPos);
		vec3 reflectDir = reflect(-lightDir, Normal);
		vec3 halfwayDir = normalize(lightDir + viewDir);  
		float spec = pow(max(dot(Normal, halfwayDir), 0.0), shininess);
		specular = diffuse * spec;
	}
	
	FragColor = ambient + diffuse * diff + specular;
}
