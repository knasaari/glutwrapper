CC=g++
LDFLAGS = -L./lib/mingw -lglfw3dll -lgdi32 -lopengl32 -lmatrix4 -lpicopng -lvec32
LDFLAGS_WIN64 = -L./lib/mingw64 -lglfw3dll -lgdi32 -lopengl32 -lmatrix4 -lpicopng64 -lvec64
LDFLAGS_LINUX = -static-libgcc -static-libstdc++ -lpthread -lGL -lglfw -ldl -L./lib -lmatrix4_32 -lpicopng_32 -lvec32
LDFLAGS_LINUX64 = -static-libgcc -static-libstdc++ -lpthread -lGL -lglfw -ldl -L./lib -lmatrix4_64 -lpicopng_64 -lvec64
CFLAGS := -std=c++11 -O2 -msse
SYS := $(shell $(CC) -dumpmachine)
ifneq (, $(findstring linux, $(SYS)))
else ifneq (, $(findstring mingw, $(SYS)))
   CFLAGS += -D MINGW
endif
CFLAGS += $(FLAGS)

BLOBS = blob/origo3d_uv.vbo blob/cup2.vbo

rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))
SOURCES := $(patsubst %.cpp,%.o, $(call rwildcard, src/, *.cpp))
DEPS := $(patsubst %.cpp,%.d, $(call rwildcard, src/, *.cpp))

all: windows

-include $(DEPS)

linux: $(SOURCES)
	$(CC) $(SOURCES) $(BLOBS) -no-pie $(CFLAGS) $(LDFLAGS_LINUX) -o ./bin/gles2

linux64: $(SOURCES)
	$(CC) $(SOURCES) $(BLOBS) -no-pie $(CFLAGS) $(LDFLAGS_LINUX64) -o ./bin/gles2

linux-mingw: $(SOURCES)
	$(CC) $(SOURCES) $(BLOBS) $(CFLAGS) $(LDFLAGS) -o ./bin/gles2.exe

windows: $(SOURCES)
	$(CC) $(SOURCES) $(BLOBS) $(CFLAGS) $(LDFLAGS) -o ./bin/gles2.exe

windows64: $(SOURCES)
	$(CC) $(SOURCES) $(BLOBS) $(CFLAGS) $(LDFLAGS_WIN64) -o ./bin/gles2.exe

%.vbo:
	./meshmaker/packbin.sh $(patsubst %.vbo,%.obj,$(@F))
	cp ./meshmaker/$(@F) $@

%.o: %.cpp
	$(CC) $(CFLAGS) -g -c -I./include $< -o $@
	$(CC) $(CFLAGS) -I./include -MM -MT$@ $< -o $(patsubst %.o,%.d,$@)
	
binaries: $(BLOBS)

clean:
	@echo $(SOURCES) $(DEPS) |xargs rm -f

clean-binaries:
	@echo $(BLOBS) |xargs rm -f

test:
	$(CC) --version
